# frozen_string_literal: true

require_relative '../triage/job'

module Triage
  class TriggerPipelineOnApprovalJob < Job
    # Unfortunately, we cannot make on demand workers from sucker_punch:
    # https://github.com/brandonhilkert/sucker_punch/blob/v3.0.1/lib/sucker_punch/queue.rb#L23-L24
    # Consider make a feature request? Would be nice to set 2:10
    workers 5

    private

    def execute(noteable_path, processor:)
      self.log_extra = { merge_request_path: noteable_path, processor: processor }
      Triage.api_client.post("#{noteable_path}/pipelines")
    end
  end
end
