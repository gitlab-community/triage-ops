# frozen_string_literal: true

require_relative '../triage/job'
require_relative '../triage/engineering_allocation_labels_validator'

module Triage
  class EngineeringAllocationLabelsReminderJob < Job
    include Reaction

    private

    def execute(event)
      prepare_executing_with(event)

      return unless validator.label_nudge_needed?

      post_reminder
    end

    def post_reminder
      message = <<~MARKDOWN.chomp
        :wave: `@#{event.event_actor_username}`, please ensure the [required labels](https://handbook.gitlab.com/handbook/product/product-processes/#engineering-allocation) are present for ~"Engineering Allocation" [measurements](https://app.periscopedata.com/app/gitlab/912243/Engineering-Allocation):

        - An `~Eng-Consumer::*` label
        - An `~Eng-Producer::*` label
        - A `~priority::*` label
        - A `~severity::*` label when the type is ~"bug"
      MARKDOWN

      add_comment(unique_comment.wrap(message), append_source_link: false)
    end

    def validator
      @validator ||= EngineeringAllocationLabelsValidator.new(event)
    end

    def unique_comment
      @unique_comment ||= validator.unique_comment
    end
  end
end
