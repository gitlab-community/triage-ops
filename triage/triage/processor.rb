# frozen_string_literal: true

require 'gitlab'

require_relative '../../lib/constants/labels'
require_relative '../triage'
require_relative 'listener'
require_relative 'percentage_rollout'
require_relative 'rate_limit'
require_relative 'reaction'
require_relative 'reactive_command'
require_relative 'sucker_punch'
require_relative 'telemetry'
require_relative 'unique_comment'

module Triage
  class Processor
    extend PercentageRollout
    extend ReactiveCommand
    include Reaction

    attr_reader :event

    def self.react_to_approvals
      # Merge request event can contain either `approved` or `approval` action
      # depending on the approval rules set for the merge request.
      # In this case, the reaction applies to either action.
      react_to 'merge_request.approval', 'merge_request.approved'
    end

    def self.react_to(*events_def)
      events_def.each do |event_def|
        listener = Listener.listeners_for_event(*event_def.split('.'))
        listeners.concat(listener)
      end
    end

    def self.listeners
      @listeners ||= []
    end

    def self.triage(event)
      new(event).triage
    end

    def initialize(event)
      @event = event
    end

    def triage
      return {} unless applicable? || comment_cleanup_applicable?

      Triage::Telemetry.record_processor_execution(event: event, processor: self) do
        if comment_cleanup_applicable?
          executed_method = 'processor_comment_cleanup'
          return_value = processor_comment_cleanup
        else
          executed_method = 'process'
          before_process
          return_value = process.tap { after_process }
        end

        { executed_method: executed_method, return_value: return_value }
      end
    end

    def process
      raise NotImplementedError
    end

    def processor_comment_cleanup
      raise NotImplementedError
    end

    def documentation
      '' # Empty by default
    end

    private

    def applicable?
      true
    end

    def comment_cleanup_applicable?
      false
    end

    def before_process
      nil
    end

    def after_process
      nil
    end

    # Override this method if a different class name is required
    def unique_comment
      @unique_comment ||= UniqueComment.new(self.class.name, event)
    end
  end
end
