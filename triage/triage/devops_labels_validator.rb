# frozen_string_literal: true
require_relative '../triage'
require_relative '../../lib/constants/labels'
require_relative '../../lib/hierarchy/group'

module Triage
  class DevopsLabelsValidator
    attr_reader :event

    PROCESSOR_CLASS = 'Triage::DevopsLabelsNudger'

    def initialize(event)
      @event = event
    end

    def labels_set?
      special_team_labels? || has_group_label? || has_category_label?
    end

    def unique_comment
      @unique_comment ||= UniqueComment.new(PROCESSOR_CLASS, event)
    end

    private

    def special_team_labels?
      (label_names & Labels::SPECIAL_ISSUE_LABELS).flatten.any?
    end

    def has_group_label?
      (Hierarchy::Group.all_labels & label_names).any?
    end

    def has_category_label?
      label_names.any? { |label_name| label_name.start_with?('Category:') }
    end

    def label_names
      @label_names ||= issue.labels
    end

    def issue
      @issue ||= Triage::Issue.new(Triage.api_client.issue(event.project_id, event.iid).to_h)
    end
  end
end
