# frozen_string_literal: true

require_relative './../label_implementation_plan'
require_relative './../label_event_finder'

module QuickWin
  module LabelValidator
    include Triage::LabelImplementationPlan

    def quick_win_label_needs_validation?
      has_empty_or_no_implementation_plan?(description) ||
        quick_win_label_older_than_one_year?
    end

    def label_event_finder
      Triage::LabelEventFinder.new({
        project_id: project_id,
        resource_iid: issue_iid,
        resource_type: 'issue',
        label_name: 'quick win',
        action: 'add'
      })
    end

    private

    def project_id
      resource[:project_id]
    end

    def issue_iid
      resource[:iid]
    end

    def one_year_ago
      Time.now - (365 * 24 * 60 * 60)
    end

    def quick_win_label_older_than_one_year?
      label_added_date < one_year_ago
    end

    def label_added_date
      label_event_finder.label_added_date || resource[:created_at]
    end
  end
end
