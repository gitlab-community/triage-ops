# frozen_string_literal: true

require_relative '../../../triage/triage'
require_relative '../../../triage/resources/ci_job'
require_relative '../../../triage/triage/pipeline_failure/config/security_stable_branch'
require_relative '../../../lib/constants/labels'
require_relative 'notified_channels'
require_relative 'duplicate_incident_manager'

module Triage
  module PipelineFailure
    class TriageIncident
      ROOT_CAUSE_LABELS = Labels::MASTER_BROKEN_ROOT_CAUSE_LABELS
      POST_RETRY_JOB_URL_THRESHOLD = 10
      RSPEC_DURATION_COMMENT_LIMIT = 800000

      def initialize(event:, config:, ci_jobs:)
        @event = event
        @config = config
        @ci_jobs = ci_jobs
      end

      def incident_labels
        labels = config.incident_labels.dup
        labels.push(top_root_cause_label, *test_level_labels)

        if top_group_label.present?
          labels << top_group_label

          labels.delete(Labels::ENGINEERING_PRODUCTIVITY_LABEL) if top_group_label != Labels::MISSING_PRODUCT_GROUP_LABEL
        end

        labels
      end

      def root_cause_analysis_comment
        comments = [
          list_all_ci_jobs_comment,
          retry_pipeline_comment,
          incident_modifier_comment
        ].compact

        return unless comments.any?

        comments.join("\n\n").prepend("\n\n")
      end

      def attribution_comment
        attributed_jobs = ci_jobs.map do |ci_job|
          <<~MARKDOWN.chomp
            #{ci_job.attribution_message_markdown}
          MARKDOWN
        end.uniq.reject(&:empty?)

        message_body = attributed_jobs.empty? ? '' : attributed_jobs.join("\n\n").prepend("\n\n")
        attributed_slack_channel_info.present? ? message_body + attributed_slack_channel_info : message_body
      end

      def investigation_comment
        return if ci_jobs.empty?

        valid_summaries = ci_jobs.map(&:test_failure_summary_markdown).reject(&:empty?)

        return if valid_summaries.empty?

        valid_summaries.join("\n\n").prepend("\n\n").chomp
      end

      def duration_analysis_comment
        jobs_with_rspec_run_time_summary = ci_jobs.filter_map(&:rspec_run_time_summary_markdown)

        return if jobs_with_rspec_run_time_summary.empty?

        string_dump = ''
        index_exceeding_limit = jobs_with_rspec_run_time_summary.reduce(0) do |index, data|
          string_dump += data
          break index if string_dump.size > rspec_duration_comment_limit

          index + 1
        end

        jobs_with_rspec_run_time_summary[0...index_exceeding_limit]
          .join("\n\n")
          .prepend("\n\n")
          .delete_suffix("\n\n")
      end

      def duplicate_incident_manager
        @duplicate_incident_manager ||= Triage::PipelineFailure::DuplicateIncidentManager.new(config: config, failed_job_names: failed_job_names)
      end

      def duplicate?
        if security_stable_branch?
          duplicate_incident_manager.canonical_incident_triggered_by_same_commit?(event)
        else
          duplicate_incident_manager.has_duplicate?
        end
      end

      def closeable?
        duplicate? || all_jobs_failed_with_transient_errors?
      end

      private

      attr_reader :event, :config, :ci_jobs

      def top_root_cause_label
        return ROOT_CAUSE_LABELS[:default] if ci_jobs.empty?

        # find the top root cause label preferably not master-broken::undetermined
        potential_root_cause_labels
          .tally
          .max_by { |label, count| label == ROOT_CAUSE_LABELS[:default] ? 0 : count }[0]
      end

      def top_group_label
        return if ci_jobs.empty? || potential_responsible_group_labels.empty?

        potential_responsible_group_labels
          .tally
          .max_by { |_label, count| count }[0]
      end

      def test_level_labels
        test_levels_with_prefixes = {
          unit: 'rspec',
          integration: 'rspec',
          migration: 'rspec',
          system: 'rspec',
          e2e: '' # E2E tests don't have a prefix
        }

        test_levels_with_prefixes.each_with_object([]) do |(test_level, prefix), labels|
          labels << Labels::RSPEC_TEST_LEVEL_LABELS[test_level] if failed_job_names.any?(/#{prefix}.*#{test_level}/)
        end
      end

      def list_all_ci_jobs_comment
        return if ci_jobs.empty?

        ci_jobs.map do |ci_job|
          %(- #{ci_job.markdown_link}: ~"#{ci_job.failure_root_cause_label}".#{retry_job_comment(ci_job)})
        end.join("\n")
      end

      def retry_job_comment(ci_job)
        retry_job_web_url = retry_job_and_return_web_url_if_applicable(ci_job)
        return if retry_job_web_url.nil?

        " Retried at: #{retry_job_web_url}"
      end

      def retry_job_and_return_web_url_if_applicable(ci_job)
        return unless transient_error?(ci_job.failure_root_cause_label) && retry_jobs_individually?

        ci_job.retry.web_url
      end

      def transient_error?(failure_root_cause_label)
        config.transient_root_cause_labels.include?(failure_root_cause_label)
      end

      def retry_jobs_individually?
        # the benefit of retrying individual job is so we can post the retried job url
        # but if there are too many failed jobs
        # it's more practical to click `retry pipeline` and verify the overall pipeline status when all jobs finish
        ci_jobs.size < POST_RETRY_JOB_URL_THRESHOLD
      end

      def retry_pipeline_comment
        return if ci_jobs.empty? || !all_jobs_failed_with_transient_errors? || retry_jobs_individually?

        retry_pipeline_response = Triage.api_client.retry_pipeline(event.project_id, event.id)

        "Retried pipeline: #{retry_pipeline_response.web_url}"
      end

      def incident_modifier_comment
        return unless closeable?

        if duplicate?
          format(config.duplicate_command_body, {
            duplicate_incident_url: duplicate_incident_manager.duplicate_source_incident.web_url
          })
        elsif ci_jobs.any?
          format(config.close_command_body)
        end
      end

      def all_jobs_failed_with_transient_errors?
        return @all_jobs_failed_with_transient_errors if defined?(@all_jobs_failed_with_transient_errors)

        @all_jobs_failed_with_transient_errors = (potential_root_cause_labels.uniq - config.transient_root_cause_labels).empty?
      end

      def potential_responsible_group_labels
        @potential_responsible_group_labels ||= ci_jobs.flat_map(&:potential_responsible_group_labels)
      end

      def potential_root_cause_labels
        @potential_root_cause_labels ||= ci_jobs.map(&:failure_root_cause_label)
      end

      def attributed_slack_channel_info
        channels_markdown = NotifiedChannels.new(config, top_group_label).to_s
        return if channels_markdown.empty?

        attribution = if top_group_label
                        "attributed to ~\"#{top_group_label}\""
                      else
                        "unattributed"
                      end

        <<~MARKDOWN.chomp.prepend("\n\n")
        **This incident is #{attribution} and posted in #{channels_markdown}.**
        MARKDOWN
      end

      def rspec_duration_comment_limit
        RSPEC_DURATION_COMMENT_LIMIT
      end

      def failed_job_names
        ci_jobs.map(&:name)
      end

      def security_stable_branch?
        config.is_a?(Triage::PipelineFailure::Config::SecurityStableBranch)
      end
    end
  end
end
