# frozen_string_literal: true

require_relative '../../../triage/triage'
require_relative 'triage_incident'
require_relative 'path_helper'
require_relative '../../../lib/constants/labels'

module Triage
  module PipelineFailure
    class IncidentCreator
      include Triage::PipelineFailure::PathHelper

      def initialize(event:, config:, failed_jobs:)
        @event = event
        @config = config
        @failed_jobs = failed_jobs
      end

      def execute
        payload = {
          issue_type: 'incident',
          description: format(config.incident_template, template_variables),
          labels: incident_labels,
          **config.incident_extra_attrs
        }

        check_for_duplicate_incident

        new_incident = Triage.api_client.create_issue(config.incident_project_id, title, payload)

        Triage.api_client.post(
          incident_discussion_path(config, new_incident),
          body: { body: root_cause_analysis_body })
        Triage.api_client.post(
          incident_discussion_path(config, new_incident),
          body: { body: investigation_body })
        Triage.api_client.post(
          incident_discussion_path(config, new_incident),
          body: { body: duration_analysis_body })

        post_duplication_and_escalation_warning

        Triage.api_client.issue(config.incident_project_id, new_incident.iid)
      end

      private

      attr_reader :event, :config, :failed_jobs

      def now
        @now ||= Time.now.utc
      end

      def title
        @title ||= begin
          full_title = "#{now.strftime('%A %F %R UTC')} - `#{event.project_path_with_namespace}` " \
                       "broken `#{event.ref}` with #{job_names_string}"

          if full_title.size >= 255
            "#{full_title[...252]}..." # max title length is 255, and we add an elipsis
          else
            full_title
          end
        end
      end

      def template_variables
        {
          project_link: project_link,
          pipeline_id: pipeline_id,
          pipeline_link: pipeline_link,
          branch_link: branch_link,
          commit_link: commit_link,
          triggered_by_link: triggered_by_link,
          pipeline_source: pipeline_source,
          pipeline_duration: pipeline_duration,
          failed_jobs_count: failed_jobs.size,
          failed_jobs_list: failed_jobs_list,
          merge_request_link: merge_request_link,
          attribution_body: attribution_body
        }
      end

      def project_link
        "[#{event.project_path_with_namespace}](#{event.project_web_url})"
      end

      def check_for_duplicate_incident
        config.auto_triage? && triager.duplicate?
      end

      def pipeline_id
        event.id
      end

      def pipeline_link
        "[##{event.id}](#{event.web_url})"
      end

      def branch_link
        "[`#{event.ref}`](#{event.project_web_url}/-/commits/#{event.ref})"
      end

      def commit_link
        "[#{event.commit_header}](#{event.project_web_url}/-/commit/#{event.sha})"
      end

      def triggered_by_link
        # Recreate the server URL from event.project_web_url...
        "[#{event.event_actor.name}](#{event.project_web_url.delete_suffix(event.project_path_with_namespace)}#{event.event_actor.username})"
      end

      def pipeline_source
        event.source
      end

      def pipeline_duration
        ((now - event.created_at) / 60.to_f).round(2)
      end

      def failed_jobs_list
        failed_jobs.map do |job|
          "- [#{job.name}](#{job.web_url}) **Job ID**: `#{job.id}` (retry with `@gitlab-bot retry_job #{job.id}`)"
        end.join("\n")
      end

      def job_names_string
        failed_jobs.map(&:name).join(', ')
      end

      def merge_request_link
        return 'N/A' unless event.merge_request

        "[#{event.merge_request.title}](#{event.merge_request.web_url})"
      end

      def incident_labels
        config.auto_triage? ? triager.incident_labels : config.incident_labels
      end

      def root_cause_analysis_body
        body = ["## Root Cause Analysis"]
        body << triager.root_cause_analysis_comment if config.auto_triage?
        body.compact.join
      end

      def investigation_body
        body = ["## Investigation Steps"]
        body << triager.investigation_comment if config.auto_triage?
        body.compact.join
      end

      def duration_analysis_body
        body = ["## Duration Analysis"]
        body << triager.duration_analysis_comment if config.auto_triage?
        body.compact.join
      end

      def attribution_body
        return unless config.auto_triage? && triager.attribution_comment

        <<~MARKDOWN
            **Attribution:**

            #{triager.attribution_comment}
        MARKDOWN
      end

      def post_duplication_and_escalation_warning
        return unless triager.duplicate_incident_manager.post_warning?

        Triage.api_client.post(
          incident_discussion_path(config, triager.duplicate_incident_manager.duplicate_source_incident),
          body: { body: triager.duplicate_incident_manager.warning_comment_body })
      end

      def triager
        @triager ||= TriageIncident.new(
          event: event,
          config: config,
          ci_jobs: failed_jobs.map do |job|
            Triage::CiJob.new(
              instance: event.instance,
              project_id: event.project_id,
              job_id: job.id,
              name: job.name,
              web_url: job.web_url
            )
          end
        )
      end
    end
  end
end
