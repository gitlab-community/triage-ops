# frozen_string_literal: true
require_relative '../triage'
require_relative '../triage/event'
require_relative '../resources/issue'
require_relative '../../lib/constants/labels'
require_relative 'unique_comment'

module Triage
  class ScheduledIssueValidator
    SPECIAL_ISSUE_LABELS = Labels::SPECIAL_ISSUE_LABELS
    TYPE_LABEL_NUDGER_CLASS = 'Triage::ScheduledIssueTypeLabelNudger'
    EXEMPTED_PROJECT_IDS    = [Triage::Event::GITLAB_DESIGN_PROJECT_ID].freeze

    attr_reader :event

    def initialize(event)
      @event = event
    end

    def type_label_nudge_needed?
      event.from_part_of_product_project? &&
        !from_exempted_project? &&
        missing_type_label? &&
        assigned_to_applicable_milestone? &&
        no_previous_type_label_comment?
    end

    def type_label_unique_comment
      @type_label_unique_comment ||= UniqueComment.new(TYPE_LABEL_NUDGER_CLASS, event)
    end

    def from_exempted_project?
      EXEMPTED_PROJECT_IDS.include?(event.project_id.to_i)
    end

    private

    def issue
      @issue ||= Triage::Issue.new(Triage.api_client.issue(event.project_id, event.iid).to_h)
    end

    def milestone
      @milestone ||= issue.milestone
    end

    def assigned_to_applicable_milestone?
      !milestone.nil? && (milestone.in_progress? || milestone.starts_within_the_next_days?(90))
    end

    def missing_type_label?
      !special_issue? &&
        (issue.labels & [*Labels::TYPE_LABELS, Labels::TYPE_IGNORE_LABEL]).empty?
    end

    def special_issue?
      (issue.labels & SPECIAL_ISSUE_LABELS).any?
    end

    def no_previous_type_label_comment?
      type_label_unique_comment.no_previous_comment?
    end
  end
end
