# frozen_string_literal: true

module Triage
  module LabelImplementationPlan
    DESCRIPTION_IMPLEMENTATION_PATTERN = /^\#{1,3}\s*implementation/i

    def has_valid_implementation_plan?(description)
      !has_empty_or_no_implementation_plan?(description)
    end

    def has_empty_or_no_implementation_plan?(description)
      description_lines = description.lines.map(&:strip).reject(&:empty?)

      return true if description_lines.empty?

      implementation_plan_title_index = description_lines.find_index do |line|
        line.match?(DESCRIPTION_IMPLEMENTATION_PATTERN)
      end

      return true unless implementation_plan_title_index

      line_index = implementation_plan_title_index + 1
      while line_index < description_lines.length
        # If we find a heading after the implementation plan title, then there's no implementation plan
        # We account for subheadings for the implementation plan that could be a third level heading
        return true if description_lines[line_index].start_with?('# ')
        return true if description_lines[line_index].start_with?('## ')

        # As soon as we find a line not starting with a hash, we consider the implementation plan to be present
        return false unless description_lines[line_index].start_with?('#')

        line_index += 1
      end
      true
    end
  end
end
