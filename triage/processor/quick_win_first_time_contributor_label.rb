# frozen_string_literal: true

require_relative '../triage/processor'
require_relative '../triage/label_implementation_plan'

module Triage
  class QuickWinFirstTimeContributorLabel < Processor
    include Triage::LabelImplementationPlan

    react_to 'issue.open', 'issue.reopen', 'issue.update'

    def applicable?
      return false unless event.from_gitlab_group?

      # Repeat the comment every time the label is added
      quick_win_first_time_contributor_label_added? && !requirements_met?
    end

    def process
      post_quick_win_first_time_contributor_label_guide_comment
    end

    def documentation
      <<~TEXT
          This processor reacts to the `quick win::first-time contributor` label being added to an issue enforcing the criteria that were
          decided as necessary requirements for the label to be applied.
          The intent is to help ensure that these issues are truly prepared for community contributions
          and can be easily executed by anyone who picks it up.
      TEXT
    end

    private

    def quick_win_first_time_contributor_label_added?
      event.added_label_names.include?(Labels::QUICK_WIN_FIRST_TIME_CONTRIBUTOR_LABEL)
    end

    def requirements_met?
      has_required_weight? && has_valid_implementation_plan?(event.description)
    end

    def has_required_weight?
      return false if event.weight.nil?

      event.weight >= 0 && event.weight <= 1
    end

    def post_quick_win_first_time_contributor_label_guide_comment
      if unique_comment.no_previous_comment?
        add_comment(unique_comment.wrap(quick_win_first_time_contributor_label_guide_comment.strip), append_source_link: true)
      else
        add_comment(labeling_commands.strip, append_source_link: false)
      end
    end

    def labeling_commands
      <<~COMMANDS
        /unlabel ~"#{Labels::QUICK_WIN_FIRST_TIME_CONTRIBUTOR_LABEL}"
        /label ~"#{Labels::AUTOMATION_QUICK_WIN_REMOVED}"
      COMMANDS
    end

    def quick_win_first_time_contributor_label_guide_comment
      <<~MARKDOWN.chomp
          @#{event.event_actor_username} thanks for adding the ~"#{Labels::QUICK_WIN_FIRST_TIME_CONTRIBUTOR_LABEL}" label!

          However, it has been automatically removed because this issue does not meet all of the required criteria for the label to be applied:
            - Must be assigned a weight between 0-1
            - Must include an implementation plan as a second or third level heading
            - Should include at least 1 GitLab team member or experienced community contributor (e.g., “Support contact: @username”) tagged in the Implementation plan section

          You can refer to the [criteria for ~"quick win::first-time contributor" issues documentation](https://handbook.gitlab.com/handbook/marketing/developer-relations/contributor-success/community-contributors-workflows/#criteria-for-quick-winfirst-time-contributor-issues) for an up-to-date list of the requirements.

         #{labeling_commands}
      MARKDOWN
    end
  end
end
