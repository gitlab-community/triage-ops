This folder contains one-off label migration policies that can be written and maintained with the assistance of [GitLab Duo Workflow](https://docs.gitlab.com/ee/user/duo_workflow/), an AI-powered coding agent in the VS Code IDE.

Refer to [this triage policy handbook page](https://handbook.gitlab.com/handbook/engineering/infrastructure-platforms/developer-experience/development-analytics/create-triage-policy-with-gitlab-duo-workflow-guide) for a comprehensive guide of writing triage policies with GitLab Duo Workflow.

### Example Prompt

Change the stage label for `group::authentication` resources to `devops::software supply chain security`:

> write a one-off label migration in policies/one-off/auth-migration.yml to apply a devops::software supply chain security label to issues , MRs and epics that are currently labeled with group::authentication. Only target open resources. Do not include any resource that already has devops::software supply chain security label.
>
> Link the new one-off policy you just created in .gitlab/ci/one-off.yml so the policy can run in CI. There needs to be 2 jobs, a dry-run and an actual job, the job names must follow the instructions listed in one-off.yml.
>
> Read instructions and example yml files in `policies/one-off/duo-workflow-guide-and-example-policies` to ensure the result has the correct syntax.

### Verification and Execution

Refer to the handbook page.

## Demo

See [this video](https://www.youtube.com/watch?v=AoCD4hh2nhc).
