.closed_recently: &closed_recently_filter
  ruby: |
      # in days; must be same or lower than `interval`
      closed_age(resource) < 7

resource_rules:
  issues:
    summaries:
      - name: Generate Growth Weekly Report
        rules:
          - name: Completed issues
            conditions:
              state: closed
              date:
                attribute: updated_at
                condition: newer_than
                interval_type: days
                interval: 7
              labels:
                - "section::growth"
                - "workflow::complete"
              <<: *closed_recently_filter
            actions:
              summarize:
                item: |
                  - [{{title}}]({{web_url}}) #{resource[:labels].grep(/^type::/).map { |label| %(~"#{label}") }.join(' ')}  #{resource[:labels].grep(/^priority::/).map { |label| %(~"#{label}") }.join(' ')}
                redact_confidential_resources: false
                summary: |
                  ## :white_check_mark: Issues delivered last week

                  {{items}}

          - name: In progress issues
            conditions:
              state: opened
              labels:
                - section::growth
                - Next Up
              ruby: |
                resource[:labels].grep(/^workflow::/).first.in?(['workflow::in dev', 'workflow::in review', 'workflow::validation'])

            actions:
              summarize:
                item: |
                  - [{{title}}]({{web_url}}) #{resource[:labels].grep(/^type::/).map { |label| %(~"#{label}") }.join(' ')}  #{resource[:labels].grep(/^priority::/).map { |label| %(~"#{label}") }.join(' ')} #{resource[:labels].grep(/^workflow::/).map { |label| %(~"#{label}") }.join(' ')}
                redact_confidential_resources: false
                summary: |
                  ## :todo: Work in progress (in dev, review or validation)

                  {{items}}

          - name: Blocked issues
            conditions:
              state: opened
              labels:
                - section::growth
                - Next Up
                - workflow::blocked
            actions:
              summarize:
                item: |
                  - [ ] [{{title}}]({{web_url}}) #{resource[:labels].grep(/^type::/).map { |label| %(~"#{label}") }.join(' ')}  #{resource[:labels].grep(/^priority::/).map { |label| %(~"#{label}") }.join(' ')}
                summary: |

                  ## :issue-blocked: Blocked issues
                  Please check issues listed here and confirm if the reason why they're blocked is still valid. Once confirmed either move the issue back to ~"workflow::ready for development" or comment on why the issue continues to be blocked.
                  {{items}}
                redact_confidential_resources: false

        actions:
          summarize:
            title: |
              #{Date.today.iso8601} Growth Weekly Report \##{Date.today.strftime("%W")}
            destination: gitlab-org/growth/team-tasks
            summary: |
              _Welcome to Growth Weekly Update \\\##{Date.today.strftime("%W")} / \\\##{Date.today.strftime("%Y")}!_

              <!-- Please add any important information and news that affect the team this week -->

              ## :newspaper2: Highlights
              -
              -

              <!-- Please add any celebrations -->
              ## :confetti_ball: Celebrations
              -
              -

              <!-- Please add themes and priorities to focus on this week -->
              ## 🎯 Themes and priorities for upcoming week
              -
              -

              {{items}}

              ## :construction: Impediments
              -
              -

              ## :moneybag: Error Budget

              - Acquisition ([7 days report](https://dashboards.gitlab.net/d/stage-groups-detail-acquisition/stage-groups-acquisition-group-error-budget-detail?orgId=1&from=now-7d&to=now) | [28 days report](https://dashboards.gitlab.net/d/stage-groups-detail-activation/stage-groups-acquisition-group-error-budget-detail?orgId=1))
              - Activation ([7 days report](https://dashboards.gitlab.net/d/stage-groups-detail-activation/stage-groups-activation-group-error-budget-detail?orgId=1&from=now-7d&to=now) | [28 days report](https://dashboards.gitlab.net/d/stage-groups-detail-activation/stage-groups-activation-group-error-budget-detail?orgId=1))

              ## Checklist
              - [ ] Update highlights (@kniechajewicz @gdoud @p_cordero)
              - [ ] Update celebrations (everyone)
              - [ ] Update product themes and priorities for the upcoming week (@gdoud @p_cordero)
              - [ ] Update engineering themes and priorities for the upcoming week (@kniechajewicz)
              - [ ] Update error budget information (@kniechajewicz)
              - [ ] Fill in any expected or existing impediments & risks or delete this section (@kniechajewicz)

              After last item on the checklist is completed:
              - [ ] Add comment `cc @justinfarris, @jeromezng, @gdoud, @p_cordero, @vincywilson, @gitlab-org/growth/engineers`

              ----
              *This report was generated from [this policy](#{ENV['CI_PROJECT_URL']}/blob/#{ENV['CI_DEFAULT_BRANCH']}/policies/groups/gitlab-org/growth/growth-weekly-report.yml).*

              /assign @kniechajewicz @gdoud @p_cordero
              /confidential
              /label ~"devops::growth" ~"type::ignore" ~"weekly update"
