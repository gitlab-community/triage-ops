# frozen_string_literal: true

require 'spec_helper'
require_relative '../../triage/processor/scheduled_issue_type_label_nudger'
require_relative '../../triage/triage/event'
require_relative '../../triage/triage/unique_comment'
require_relative '../../triage/resources/issue'

RSpec.describe Triage::ScheduledIssueTypeLabelNudger do
  include_context 'with event', Triage::IssueEvent do
    let(:added_milestone_id) { 123 }
    let(:date_today)   { Date.today }
    let(:today)        { date_today.to_s }
    let(:in_30_days)   { (date_today + 30).to_s }

    let(:event_attrs) do
      { object_kind: Triage::Event::ISSUE,
        from_part_of_product_project?: true,
        added_milestone_id: added_milestone_id }
    end

    let(:milestone_attr) do
      { 'id' => added_milestone_id,
        'project_id' => project_id,
        'start_date' => today,
        'due_date' => in_30_days }
    end

    let(:issue_attr) do
      { 'iid' => iid, 'milestone' => milestone_attr, 'labels' => label_names }
    end
  end

  let(:issue_validator) { subject.__send__(:validator) }
  let(:type_label_nudge_needed) { true }
  let(:unique_comment) { Triage::UniqueComment.new(described_class.to_s) }
  let(:previous_comment) { nil }

  subject { described_class.new(event) }

  before do
    stub_api_request(
      path: "/projects/#{project_id}/issues/#{iid}",
      response_body: issue_attr
    )

    stub_api_request(
      path: "/projects/#{project_id}/issues/#{iid}/notes",
      query: { per_page: Triage::UniqueComment::NOTES_PER_PAGE },
      response_body: [previous_comment].compact
    )

    stub_api_request(
      path: "/projects/#{project_id}/milestones/#{added_milestone_id}",
      response_body: milestone_attr
    )

    allow(issue_validator).to receive(:type_label_nudge_needed?).and_return(type_label_nudge_needed)
  end

  include_examples 'registers listeners', ['issue.open', 'issue.update']

  include_examples 'applicable on contextual event'

  describe '#applicable?' do
    context 'when event did not change milestone_id' do
      let(:added_milestone_id) { nil }

      include_examples 'event is not applicable'
    end

    context 'when event changed milestone_id' do
      context 'when issue does not need type label nudge' do
        let(:type_label_nudge_needed) { false }

        include_examples 'event is not applicable'
      end

      context 'when issue needs type label nudge' do
        include_examples 'event is applicable'
      end
    end
  end

  describe '#comment_cleanup_applicable?' do
    context 'with no previous comment' do
      include_examples 'event does not require process cleanup'
    end

    context 'with previous comment' do
      let(:previous_comment) do
        {
          id: 1,
          author: { 'username' => 'gitlab-bot' },
          body: '<!-- triage-serverless ScheduledIssueTypeLabelNudger -->'
        }
      end

      context 'when resource needs type label nudge' do
        include_examples 'event does not require process cleanup'
      end

      context 'when resource does not need type label nudge' do
        let(:type_label_nudge_needed) { false }

        include_examples 'event requires process cleanup'
      end
    end
  end

  describe 'processor_comment_cleanup' do
    it 'deletes previous comment' do
      expect(issue_validator.type_label_unique_comment).to receive(:delete_previous_comment)

      subject.processor_comment_cleanup
    end
  end

  describe '#documentation' do
    it_behaves_like 'processor documentation is present'
  end

  describe '#process' do
    it 'schedules a ScheduledIssueTypeLabelNudgerJob 5 minutes later' do
      expect(
        Triage::ScheduledIssueTypeLabelNudgerJob
      ).to receive(:perform_in).with(described_class::FIVE_MINUTES, event)

      subject.process
    end
  end
end
