# frozen_string_literal: true

require 'spec_helper'
require_relative '../../../triage/processor/support/request_for_help_lifecycle'

RSpec.describe Triage::RequestForHelpLifecycle do
  let(:resource_author_id) { 123 }
  let(:event_actor_id) { 456 }
  let(:event_actor_username) { 'support-person' }
  let(:existing_labels) { [] }
  let(:project_id) { described_class::RFH_PROJECT_ID }

  include_context 'with event', Triage::IssueNoteEvent do
    let(:event_attrs) do
      {
        object_kind: 'note',
        action: 'create',
        label_names: existing_labels,
        event_actor_id: event_actor_id,
        event_actor_username: event_actor_username,
        resource_author_id: resource_author_id,
        project_id: project_id
      }
    end
  end

  subject(:test_event) { described_class.new(event) }

  before do
    allow(Triage).to receive(:gitlab_com_support_member_ids).and_return([123, 456])
  end

  describe '#applicable?' do
    context 'when the project ID matches RFH project' do
      let(:project_id) { described_class::RFH_PROJECT_ID }

      context 'when the event is coming from a known bot' do
        let(:event_actor_id) { Triage::Event::GITLAB_BOT_ID }

        include_examples 'event is not applicable'
      end

      context 'when the event is coming from a potential bot' do
        let(:event_actor_username) { 'project_64541115_bot_b2ff0f44093db7a8642e95ac1295114a' }

        include_examples 'event is not applicable'
      end

      context 'when author is a support team member' do
        include_examples 'event is applicable'
      end

      context 'when author is not a support team member' do
        let(:resource_author_id) { 999 }

        include_examples 'event is not applicable'
      end
    end

    context 'when project ID does not match RFH project' do
      let(:project_id) { 999999 }

      include_examples 'event is not applicable'
    end

    context 'when the RFH is triaged by a Support SME' do
      let(:existing_labels) { [Labels::TRIAGE_BY_SUPPORT_LABEL] }

      include_examples 'event is not applicable'
    end
  end

  describe '#process' do
    context 'when a new comment is added' do
      context 'when comment author is a support team member' do
        let(:event_actor_id) { 123 }

        it 'adds LAST_COMMENT_SUPPORT_TEAM_LABEL label comment' do
          body = <<~MARKDOWN.chomp
            /label ~"#{Labels::LAST_COMMENT_SUPPORT_TEAM_LABEL}"
          MARKDOWN

          expect_comment_request(event: event, body: body) do
            test_event.process
          end
        end

        context 'when label already exists' do
          let(:existing_labels) { [Labels::LAST_COMMENT_SUPPORT_TEAM_LABEL] }

          it 'no comment is posted' do
            expect_no_request { test_event.process }
          end
        end
      end

      context 'when comment author is not a support team member' do
        let(:event_actor_id) { 999 }

        it 'adds LAST_COMMENT_DEV_TEAM_LABEL label comment' do
          body = <<~MARKDOWN.chomp
            /label ~"#{Labels::LAST_COMMENT_DEV_TEAM_LABEL}"
          MARKDOWN

          expect_comment_request(event: event, body: body) do
            test_event.process
          end
        end

        context 'when label already exists' do
          let(:existing_labels) { [Labels::LAST_COMMENT_DEV_TEAM_LABEL] }

          it 'no comment is posted' do
            expect_no_request { test_event.process }
          end
        end
      end
    end

    context 'when a RFH issue is opened' do
      include_context 'with event', Triage::IssueEvent do
        let(:event_attrs) do
          {
            object_kind: 'issue',
            action: 'open',
            label_names: [],
            event_actor_id: event_actor_id,
            resource_author_id: event_actor_id,
            project_id: described_class::RFH_PROJECT_ID
          }
        end
      end

      context 'when issue author is support team member' do
        let(:resource_author_id) { 123 }

        it 'adds NEWLY_OPENED_RFH_TEAM_LABEL label comment' do
          body = <<~MARKDOWN.chomp
            /label ~"#{Labels::NEWLY_OPENED_RFH_TEAM_LABEL}"
          MARKDOWN

          expect_comment_request(event: event, body: body) do
            test_event.process
          end
        end
      end
    end
  end
end
