# frozen_string_literal: true

require 'spec_helper'

require_relative '../../triage/processor/engineering_allocation_labels_reminder'
require_relative '../../triage/triage/event'

RSpec.describe Triage::EngineeringAllocationLabelsReminder do
  include_context 'with event', Triage::IssueEvent do
    let(:event_attrs) do
      {
        object_kind: 'issue',
        action: 'update',
        label_names: label_names,
        added_label_names: added_label_names,
        from_gitlab_org?: true,
        url: 'https://url'
      }
    end

    let(:label_names) { added_label_names }
    let(:required_labels) do
      [
        'Eng-Consumer::Marketing',
        'Eng-Producer::Marketing',
        'priority::1',
        'severity::1',
        'bug'
      ]
    end
  end

  let(:notes) { [] }
  let(:previous_comment) do
    { 'id' => 1, 'body' => '<!-- triage-serverless EngineeringAllocationLabelsReminder -->' }
  end

  subject { described_class.new(event) }

  before do
    stub_api_request(
      verb: :get,
      path: "/projects/#{event.project_id}/issues/#{event.iid}/notes",
      query: { per_page: 100 },
      response_body: notes
    )

    stub_api_request(
      verb: :get,
      path: "/projects/#{event.project_id}/issues/#{event.iid}",
      response_body: {
        'iid' => iid,
        'labels' => label_names
      }
    )
  end

  include_examples 'registers listeners', ["issue.open", "issue.update"]

  describe '#applicable?' do
    context 'when event project is not under gitlab-org' do
      before do
        allow(event).to receive(:from_gitlab_org?).and_return(false)
      end

      include_examples 'event is not applicable'
    end

    context 'when other label is added' do
      let(:added_label_names) { ['bug'] }

      include_examples 'event is not applicable'
    end

    context 'when no label is added' do
      include_examples 'event is not applicable'
    end

    context 'when ~"Engineering Allocation" label is added' do
      let(:added_label_names) { ['Engineering Allocation'] }

      context 'when issue has required labels' do
        let(:label_names) { required_labels }

        include_examples 'event is not applicable'
      end

      context 'when issue is missing required labels' do
        let(:label_names) { ['bug'] }

        context 'when there is a previous comment' do
          let(:notes) { [previous_comment] }

          include_examples 'event is not applicable'
        end

        context 'when there is no previous comment' do
          include_examples 'event is applicable'
        end
      end
    end
  end

  describe '#comment_cleanup_applicable?' do
    context 'when there is no previous comment' do
      include_examples 'event does not require process cleanup'
    end

    context 'when there is a previous comment' do
      let(:notes) { [previous_comment] }

      context 'when the issue does not have required labels' do
        include_examples 'event does not require process cleanup'
      end

      context 'when the issue has required labels' do
        let(:label_names) { required_labels }

        include_examples 'event requires process cleanup'
      end
    end
  end

  describe '#documentation' do
    it_behaves_like 'processor documentation is present'
  end

  describe '#process' do
    it 'schedules a EngineeringAllocationLabelsReminderJob 5 minutes later' do
      expect(Triage::EngineeringAllocationLabelsReminderJob).to receive(:perform_in).with(described_class::FIVE_MINUTES, event)

      subject.process
    end
  end

  describe '#processor_comment_cleanup' do
    let(:notes) { [previous_comment] }

    it 'deletes previous comment' do
      expect_api_request(
        verb: :delete,
        path: "/projects/#{event.project_id}/issues/#{event.iid}/notes/1"
      ) do
        subject.processor_comment_cleanup
      end
    end
  end
end
