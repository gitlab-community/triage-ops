# frozen_string_literal: true

require 'spec_helper'

require_relative '../../triage/processor/ux_paper_cuts_mrs'
require_relative '../../triage/triage/event'

RSpec.describe Triage::UxPaperCutsMrs do
  include_context 'with slack posting context'
  include_context 'with event', Triage::MergeRequestEvent do
    let(:event_attrs) do
      {
        from_gitlab_org?: true,
        url: url,
        title: title
      }
    end

    let(:url) { 'http://gitlab.com/mr_url' }
    let(:title) { 'Merge request title' }
    let(:label_names) { [described_class::UX_PAPER_CUTS_LABEL] }
  end

  subject { described_class.new(event, messenger: messenger_stub) }

  before do
    stub_api_request(
      path: "/projects/#{event.project_id}/#{event.object_kind}s/#{event.iid}/notes",
      query: { per_page: 100 },
      response_body: [])
  end

  include_examples 'registers listeners', ["merge_request.merge"]

  it_behaves_like 'processor slack options', '#ux_paper_cuts_mrs'

  include_examples 'applicable on contextual event'

  describe '#applicable?' do
    context 'when event project is not under gitlab-org' do
      before do
        allow(event).to receive(:from_gitlab_org?).and_return(false)
      end

      include_examples 'event is not applicable'
    end

    context 'when other label is added' do
      let(:label_names) { ['bug'] }

      include_examples 'event is not applicable'
    end

    context 'when no label is added' do
      let(:label_names) { [] }

      include_examples 'event is not applicable'
    end
  end

  describe '#process' do
    before do
      allow(messenger_stub).to receive(:ping)
    end

    it_behaves_like 'slack message posting' do
      let(:message_body) do
        <<~MARKDOWN
          An MR from the UX Paper Cuts team was just merged 🎉 (#{title}) - #{url}.
        MARKDOWN
      end
    end
  end
end
