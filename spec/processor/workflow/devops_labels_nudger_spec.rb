# frozen_string_literal: true

require 'spec_helper'

require_relative '../../../triage/processor/workflow/devops_labels_nudger'
require_relative '../../../triage/triage/event'

RSpec.describe Triage::Workflow::DevopsLabelsNudger do
  include_context 'with event', Triage::IssueEvent do
    let(:event_attrs) do
      {
        new_entity?: new_entity,
        object_kind: 'issue',
        by_team_member?: true,
        from_gitlab_org_gitlab?: true
      }
    end
  end

  let(:label_names) { [] }
  let(:notes) { [] }
  let(:new_entity) { true }
  let(:previous_comment) do
    { 'id' => 1,
      'body' => '<!-- triage-serverless DevopsLabelsNudger -->' }
  end

  subject { described_class.new(event) }

  before do
    stub_api_request(
      path: "/projects/#{project_id}/issues/#{iid}",
      response_body: { 'iid' => iid, 'labels' => label_names }
    )

    stub_api_request(
      path: "/projects/#{project_id}/issues/#{iid}/notes",
      query: { per_page: 100 },
      response_body: notes
    )
  end

  describe '#applicable?' do
    it_behaves_like 'applicable on contextual event'

    context 'when event action is not from opening a new entity' do
      let(:new_entity) { false }

      it_behaves_like 'event is not applicable'
    end

    context 'when event labels include group label' do
      let(:label_names) { ['group::group1'] }

      include_examples 'event is not applicable'
    end

    context 'when event labels include a Category label' do
      let(:label_names) { ['Category:API'] }

      include_examples 'event is not applicable'
    end

    context 'when event labels include special team label' do
      let(:label_names) { ['meta'] }

      include_examples 'event is not applicable'
    end

    context 'when issue has a previous coment' do
      let(:notes) { [previous_comment] }

      include_examples 'event is not applicable'
    end
  end

  describe '#process' do
    it 'schedules a DevopsLabelsNudgerJob 5 minutes later' do
      expect(Triage::DevopsLabelsNudgerJob).to receive(:perform_in).with(
        Triage::DEFAULT_ASYNC_DELAY_MINUTES, event
      )

      subject.process
    end
  end

  describe '#documentation' do
    it_behaves_like 'processor documentation is present'
  end

  describe '#comment_cleanup_applicable?' do
    context 'with no previous comment' do
      include_examples 'event does not require process cleanup'
    end

    context 'with a previous comment' do
      let(:notes) { [previous_comment] }

      context 'when devops labels missing' do
        include_examples 'event does not require process cleanup'
      end

      context 'when event labels include group label' do
        let(:label_names) { ['group::group1'] }

        it_behaves_like 'event requires process cleanup'
      end
    end
  end

  describe '#processor_comment_cleanup' do
    context 'with no previous comment' do
      it 'returns nil' do
        expect(subject.processor_comment_cleanup).to be_nil
      end
    end

    context 'with a previous comment' do
      let(:notes) { [previous_comment] }

      it 'deletes the previous comment' do
        expect(Triage.api_client).to receive(:delete).with("/projects/#{project_id}/issues/#{iid}/notes/1")

        subject.processor_comment_cleanup
      end
    end
  end
end
