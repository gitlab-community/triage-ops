# frozen_string_literal: true

require 'spec_helper'

require_relative '../../../triage/processor/community/detect_and_flag_spam'

RSpec.describe Triage::DetectAndFlagSpam do
  include_context 'with event', Triage::MergeRequestEvent do
    let(:event_attrs) do
      {
        title: "This is a spam MR",
        description: "Random words situs demo slot"
      }
    end

    let(:label_names) { ['Community contribution'] }
  end

  subject { described_class.new(event) }

  include_examples 'registers listeners', ['merge_request.open', 'merge_request.update']

  describe '#applicable?' do
    it_behaves_like 'community contribution open resource #applicable?'

    include_examples 'event is applicable'

    context 'when ~Spam label is present' do
      let(:label_names) { super() + [Labels::SPAM_LABEL] }

      include_examples 'event is not applicable'
    end

    context 'when merge request does not contain a spam payload' do
      let(:event_attrs) do
        {
          title: "This is not spam",
          description: "There's a whole lot of description here"
        }
      end

      include_examples 'event is not applicable'
    end
  end

  describe '#documentation' do
    it_behaves_like 'processor documentation is present'
  end

  describe '#process' do
    it 'posts a message to label Spam' do
      body = add_automation_suffix do
        <<~MARKDOWN.chomp
          This merge request has been found to contain content that is inappropriate and violates the
          [GitLab Website Terms of Use](https://handbook.gitlab.com/handbook/legal/policies/website-terms-of-use/).

          Closing and marking appropriately.

          /relabel ~"Spam"
          /close
        MARKDOWN
      end

      expect_comment_request(event: event, body: body) do
        subject.process
      end
    end
  end
end
