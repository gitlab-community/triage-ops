# frozen_string_literal: true

require 'spec_helper'

require 'rack/test'

require_relative '../../triage/rack/app'

RSpec.describe Triage::Rack::App do
  include Rack::Test::Methods

  describe '#app' do
    def app
      described_class.app
    end

    describe '/dashboard' do
      context 'when authenticated' do
        let(:token) { 'token' }

        before do
          stub_env('GITLAB_DASHBOARD_TOKEN' => token)

          authorize('', token)
        end

        it 'responds SuckerPunch::Queue.stats as JSON' do
          get('/dashboard')

          expect(JSON.parse(last_response.body)).to eq(SuckerPunch::Queue.stats)
          expect(last_response).to be_ok
        end
      end

      context 'when unauthenticated' do
        it 'responds 401' do
          get('/dashboard')

          expect(last_response).to be_unauthorized
        end
      end
    end
  end
end
