# frozen_string_literal: true

require 'spec_helper'

require_relative '../../triage/job/docs_only_labeller_job'
require_relative '../../triage/triage/event'
require_relative '../../lib/constants/labels'
require_relative '../../triage/triage/changed_file_list'

RSpec.describe Triage::DocsOnlyLabellerJob do
  include_context 'with event', Triage::MergeRequestEvent

  describe '#perform' do
    using RSpec::Parameterized::TableSyntax

    let(:project_id) { 133 }
    let(:iid) { 331 }
    let(:changed_file_list) { double('Triage::ChangedFileList') }
    let(:labels) { [] }
    let(:merge_request) { double('Triage::MergeRequest') }

    # rubocop:disable Lint/BinaryOperatorWithIdenticalOperands
    where(:docs_only_label, :backend_label, :only_docs_changes, :expected) do
      false | false | false | nil
      false | false | true  | '/label ~docs-only'
      false | true  | false | nil
      false | true  | true  | nil
      true  | false | false | '/unlabel ~docs-only'
      true  | false | true  | nil
      true  | true  | false | '/unlabel ~docs-only'
      true  | true  | true  | '/unlabel ~docs-only'
    end
    # rubocop:enable Lint/BinaryOperatorWithIdenticalOperands

    with_them do
      before do
        allow(Triage::ChangedFileList).to receive(:new).and_return(changed_file_list)
        allow(changed_file_list).to receive(:only_change?).with(described_class::DOCS_ONLY_REGEX).and_return(only_docs_changes)

        labels << "docs-only" if docs_only_label
        labels << "backend" if backend_label

        stub_api_request(
          path: "/projects/#{project_id}/merge_requests/#{iid}",
          response_body: { labels: labels })
      end

      it 'applies the relevant quick action' do
        if expected
          expect_comment_request(event: event, body: expected) do
            subject.perform(event)
          end
        else
          expect_no_request { subject.perform(event) }
        end
      end
    end
  end

  describe 'regex' do
    it 'matches a file in the doc folder' do
      expect('doc/file.md'.match?(described_class::DOCS_ONLY_REGEX)).to be_truthy
    end

    it 'matches a file in the docs folder' do
      expect('docs/file.md'.match?(described_class::DOCS_ONLY_REGEX)).to be_truthy
    end

    it 'does not match a file starting with doc' do
      expect('dockerfile'.match?(described_class::DOCS_ONLY_REGEX)).to be_falsy
    end
  end
end
