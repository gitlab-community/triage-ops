# frozen_string_literal: true

require 'spec_helper'

require_relative '../../triage/job/devops_labels_nudger_job'
require_relative '../../triage/triage/event'
require_relative '../../triage/triage/unique_comment'

RSpec.describe Triage::DevopsLabelsNudgerJob do
  include_context 'with event', Triage::IssueEvent do
    let(:event_attrs) do
      { object_kind: 'issue',
        project_id: project_id,
        from_gitlab_org_gitlab?: from_gitlab_org_gitlab,
        by_team_member?: by_team_member,
        event_actor_username: 'username',
        label_names: label_names }
    end
  end

  let(:project_id) { Triage::Event::GITLAB_PROJECT_ID }
  let(:from_gitlab_org_gitlab) { true }
  let(:by_team_member)       { true }
  let(:label_names)          { [] }
  let(:issue_path)           { "/projects/#{project_id}/issues/#{iid}" }
  let(:issue_notes_api_path) { "#{issue_path}/notes" }
  let(:notes)                { [] }

  let(:unique_comment) do
    Triage::UniqueComment.new('Triage::DevopsLabelsNudger', event)
  end

  let(:missing_devops_labels_comment_request_body) do
    add_automation_suffix do
      <<~MARKDOWN.chomp
        <!-- triage-serverless DevopsLabelsNudger -->
        :wave: @username, #{described_class::LABELS_MISSING_MESSAGE}
      MARKDOWN
    end
  end

  subject { described_class.new }

  before do
    stub_api_request(
      path: issue_path,
      response_body: { labels: label_names }
    )

    stub_api_request(
      path: "#{issue_notes_api_path}?per_page=100",
      response_body: notes
    )
  end

  describe '#perform' do
    let(:previous_comment) { [] }

    context 'when issue does not require any label change' do
      let(:label_names) { ['group::pipeline security'] }

      it 'does not post any comment' do
        expect_no_request { subject.perform(event) }
      end
    end

    context 'when issue require label change' do
      it 'posts a nudge comment' do
        expect_api_requests do |requests|
          requests << stub_api_request(
            path: "#{issue_notes_api_path}?per_page=100",
            response_body: previous_comment
          )
          requests << stub_comment_request(
            event: event,
            body: missing_devops_labels_comment_request_body
          )

          subject.perform(event)
        end
      end
    end

    context 'with a previous comment' do
      let(:notes) { [previous_comment] }
      let(:previous_comment) do
        [{
          body: missing_devops_labels_comment_request_body,
          author: { username: 'gitlab-serverless-triage-bot' }
        }]
      end

      it 'fetches the previous comments but does not post comment' do
        expect_api_request(
          path: issue_notes_api_path,
          query: { per_page: 100 },
          response_body: previous_comment
        ) do
          subject.perform(event)
        end
      end

      context 'with a different project' do
        let(:from_gitlab_org_gitlab) { false }

        it 'does nothing' do
          expect_no_request(
            verb: :post,
            request_body: { body: missing_devops_labels_comment_request_body },
            path: issue_notes_api_path,
            response_body: {}
          ) do
            subject.perform(event)
          end
        end

        context 'with a different user' do
          let(:by_team_member) { false }

          it 'does nothing' do
            expect_no_request { subject.perform(event) }
          end
        end
      end
    end
  end
end
