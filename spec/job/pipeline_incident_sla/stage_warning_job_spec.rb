# frozen_string_literal: true

require 'spec_helper'

require_relative '../../../triage/job/pipeline_incident_sla/stage_warning_job'
require_relative '../../../triage/triage/event'
require_relative './pipeline_incident_sla_job_shared_context'

RSpec.describe Triage::PipelineIncidentSla::StageWarningJob do
  include_context 'with event', Triage::IssueEvent do
    let(:event_attrs) do
      {
        label_names: label_names,
        url: 'incident_web_url'
      }
    end
  end

  include_context 'with pipeline incident sla job'

  let(:label_names) { ['master:broken', 'group::pipeline security', 'escalation::needed'] }

  describe '#perform' do
    context 'when incident is not attributed to any group' do
      let(:label_names) { ['master:broken', 'escalation::needed'] }

      it 'does nothing' do
        expect_no_request { subject.perform(event) }
      end
    end

    context 'when incident is closed' do
      let(:state) { 'closed' }

      it 'does nothing' do
        expect_no_request { subject.perform(event) }
      end
    end

    context 'when incident label has none of the escalation::xx label' do
      let(:label_names) { ['master:broken'] }

      it 'does nothing' do
        expect_no_request { subject.perform(event) }
      end
    end

    context 'when incident is labeled with escalation::skipped' do
      let(:label_names) { ['group::pipeline security', 'escalation::skipped'] }

      it 'does nothing' do
        expect_no_request { subject.perform(event) }
      end
    end

    context 'when incident has escalation::needed label' do
      let(:label_names) { ['group::pipeline security', 'escalation::needed'] }
      let(:today) { Date.new(2024, 10, 22) } # Tuesday

      before do
        Timecop.freeze(today)
      end

      after do
        Timecop.return
      end

      shared_examples 'proceed with escalation ping' do |expected_channel|
        let(:slack_client_double) { instance_double(Slack::Messenger) }
        let(:webhook) { 'slack_webhook_url' }

        before do
          stub_env('SLACK_WEBHOOK_URL', webhook)
          allow(Slack::Messenger).to receive(:new).with(
            webhook,
            { channel: expected_channel,
              username: 'gitlab-bot',
              icon_emoji: ':gitlab-bot:' }
          ).and_return(slack_client_double)
        end

        it "sends a Slack reminder to #{expected_channel}" do
          expect(slack_client_double).to receive(:ping).with(text: expected_text)

          subject.perform(event)
        end
      end

      context 'when today is a weekday' do
        let(:expected_text) do
          <<~MESSAGE.chomp
            I'm requesting help on behalf of `#g_pipeline-security_alerts` to triage a recurring pipeline incident <incident_web_url|#456>.
            The incident has not received any update and will be escalated to <##{described_class::DEV_ESCALATION_CHANNEL_ID}> in 20 minutes of inactivity (except weekends and holidays).
            Thanks for your attention!
          MESSAGE
        end

        it_behaves_like 'proceed with escalation ping', 'stage_with_two_groups_channel'

        context 'when stage channel is not found' do
          let(:label_names) { ['Contributor Success', 'escalation::needed'] }
          let(:expected_text) do
            <<~MESSAGE.chomp
              I'm requesting help on behalf of `#contributor-success` to triage a recurring pipeline incident <incident_web_url|#456>.
              The incident has not received any update and will be escalated to <##{described_class::DEV_ESCALATION_CHANNEL_ID}> in 20 minutes of inactivity (except weekends and holidays).
              Thanks for your attention!
            MESSAGE
          end

          it_behaves_like 'proceed with escalation ping', 'development'
        end

        context 'when stage is not found' do
          let(:label_names) { ['group::gitaly', 'escalation::needed'] } # mocked stages.json is missing the systems stage
          let(:expected_text) do
            <<~MESSAGE.chomp
              I'm requesting help on behalf of `#gitaly-alerts` to triage a recurring pipeline incident <incident_web_url|#456>.
              The incident has not received any update and will be escalated to <##{described_class::DEV_ESCALATION_CHANNEL_ID}> in 20 minutes of inactivity (except weekends and holidays).
              Thanks for your attention!
            MESSAGE
          end

          it_behaves_like 'proceed with escalation ping', 'development'
        end

        context 'when attributed team does not belong to any stage' do
          let(:label_names) { ['Engineering Productivity', 'escalation::needed'] }
          let(:expected_text) do
            <<~MESSAGE.chomp
              I'm requesting help on behalf of `#g_engineering_productivity` to triage a recurring pipeline incident <incident_web_url|#456>.
              The incident has not received any update and will be escalated to <##{described_class::DEV_ESCALATION_CHANNEL_ID}> in 20 minutes of inactivity (except weekends and holidays).
              Thanks for your attention!
            MESSAGE
          end

          it_behaves_like 'proceed with escalation ping', 'development'
        end
      end

      context 'when today is a weekend' do
        let(:today) { Date.new(2024, 10, 20) } # Sunday

        context 'when attributed team does not belong to any stage' do
          let(:label_names) { ['Engineering Productivity', 'escalation::needed'] }
          let(:expected_text) do
            <<~MESSAGE.chomp
              I'm requesting help on behalf of `#g_engineering_productivity` to triage a recurring pipeline incident <incident_web_url|#456>.
              If it is blocking any urgent deployment or merge request for the weekend, please escalate to <##{described_class::DEV_ESCALATION_CHANNEL_ID}>.
              Otherwise, please help triage the incident on your next working day.
              Thanks for your attention!
            MESSAGE
          end

          it_behaves_like 'proceed with escalation ping', 'development'
        end
      end
    end
  end
end
