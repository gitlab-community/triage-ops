# frozen_string_literal: true

require 'spec_helper'
require_relative '../../triage/triage/event'
require_relative '../../triage/triage/devops_labels_validator'
require_relative '../../triage/resources/issue'

RSpec.describe Triage::DevopsLabelsValidator do
  include_context 'with event', Triage::IssueEvent do
    let(:event_attrs) do
      {
        object_kind: 'issue',
        by_team_member?: true,
        from_gitlab_org_gitlab?: true,
        label_names: label_names
      }
    end
  end

  let(:label_names) { [] }

  subject { described_class.new(event) }

  before do
    stub_api_request(
      path: "/projects/#{project_id}/issues/#{iid}",
      response_body: { 'iid' => iid, 'labels' => label_names }
    )
  end

  describe 'labels_set?' do
    context 'with no label' do
      let(:label_names) { [] }

      it 'returns false' do
        expect(subject.labels_set?).to be false
      end
    end

    context 'with section label' do
      let(:label_names) { ['section::growth'] }

      it 'returns false' do
        expect(subject.labels_set?).to be false
      end
    end

    context 'with group label' do
      let(:label_names) { ['group::group1'] }

      it 'returns true' do
        expect(subject.labels_set?).to be true
      end
    end

    context 'with category label' do
      let(:label_names) { ['Category:Groups & Projects'] }

      it 'returns true' do
        expect(subject.labels_set?).to be true
      end
    end

    context 'with special team label' do
      let(:label_names) { ['meta'] }

      it 'returns true' do
        expect(subject.labels_set?).to be true
      end
    end
  end
end
