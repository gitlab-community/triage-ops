# frozen_string_literal: true

require 'spec_helper'

require_relative '../../triage/triage/job'

RSpec.describe Triage::Job do
  let(:job) do
    Class.new(described_class) do
      def execute; end
    end.new
  end

  describe '#perform' do
    let(:start_time) { double(:start_time) }
    let(:end_time) { double(:end_time) }
    let(:duration) { double(:duration) }

    before do
      expect(job).to receive(:execute).and_call_original

      expect(Triage).to receive(:current_monotonic_time)
        .and_return(start_time).ordered

      expect(Triage).to receive(:current_monotonic_time)
        .and_return(end_time).ordered

      expect(end_time).to receive(:-).with(start_time).and_return(duration)

      allow(duration).to receive(:round).and_return(duration)
    end

    it 'logs the duration for running #execute' do
      expect(job.logger).to receive(:info)
        .with(
          'Executing job end',
          dry_run: Triage.dry_run?,
          job: job.class,
          duration: duration,
          event_class: nil,
          event_key: nil,
          event_payload: nil,
          log_extra: nil
        )

      job.perform
    end

    context 'when #prepare_executing_with is being called in #execute' do
      let(:job) do
        Class.new(described_class) do
          def execute(event)
            prepare_executing_with(event)
          end
        end.new
      end

      let(:event) do
        double(
          url: 'url',
          class: 'class',
          key: 'key',
          payload: 'payload')
      end

      it 'memoizes the event and logs event data as well' do
        expect(job.logger).to receive(:info)
          .with(
            'Executing job',
            job: job.class,
            resource_url: event.url)

        expect(job.logger).to receive(:info)
          .with(
            'Executing job end',
            dry_run: Triage.dry_run?,
            job: job.class,
            duration: duration,
            event_class: event.class,
            event_key: event.key,
            event_payload: event.payload,
            log_extra: nil
          )

        job.perform(event)
      end
    end
  end
end
