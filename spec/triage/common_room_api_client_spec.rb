# frozen_string_literal: true

require 'spec_helper'

require_relative '../../triage/triage/common_room_api_client'

RSpec.describe CommonRoomApiClient do
  let(:body) { { activityType: 'activityType', id: 11 } }
  let(:token) { 'crtoken' }
  let(:headers) do
    {
      'Content-Type' => 'application/json',
      'Authorization' => "Bearer #{token}"
    }
  end

  before do
    stub_env('COMMON_ROOM_API_TOKEN', token)
    stub_request(:post, "#{described_class.base_uri}/source/22150/activity")
    .to_return(status: http_status, body: response_body.to_json)
  end

  describe '.add_activity' do
    context 'when the API call succeeds' do
      let(:http_status)   { 200 }
      let(:response_body) { {} }

      it 'makes a http post to the expected endpoint with the expected headers and payload' do
        described_class.new.add_activity(body)
      end
    end

    context 'when the API call returns a failure' do
      let(:http_status)   { 403 }

      context 'when an HTTP response body is present' do
        let(:response_body) { { reason: "Token not valid" } }

        it 'throws an error' do
          expect { described_class.new.add_activity(body) }.to raise_error(
            StandardError,
            'Error sending activityType-11 to Common Room: {"reason":"Token not valid"} (HTTP code: 403)'
          )
        end
      end

      context 'when an HTTP response body is absent' do
        let(:response_body) { {} }

        it 'throws an error' do
          expect { described_class.new.add_activity(body) }.to raise_error(
            StandardError,
            'Error sending activityType-11 to Common Room: {} (HTTP code: 403)'
          )
        end
      end
    end
  end
end
