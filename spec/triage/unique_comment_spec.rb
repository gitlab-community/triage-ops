# frozen_string_literal: true

require 'spec_helper'

require_relative '../../triage/triage/event'
require_relative '../../triage/triage/processor'
require_relative '../../triage/triage/unique_comment'

RSpec.describe Triage::UniqueComment do
  include_context 'with event' do
    let(:event_attrs) do
      {
        object_kind: 'issue',
        action: 'open'
      }
    end
  end

  let(:note_author) { 'someone' }

  def expect_notes_request_with(event, body:, &block)
    response_body =
      if body
        [{ body: body, author: { username: note_author } }]
      else
        []
      end

    expect_api_request(
      path: "/projects/#{event.project_id}/#{event.object_kind}s/#{event.iid}/notes",
      query: { per_page: 100 },
      response_body: response_body,
      &block
    )
  end

  subject { described_class.new('namespace::TestClass', event) }

  describe '#initialize' do
    context 'when no class_name is passed as an argument' do
      let(:instance) { described_class.new }

      it 'raises an error' do
        expect { instance }.to raise_error(ArgumentError)
      end
    end

    context 'when no event is passed as an argument' do
      let(:instance) { described_class.new('namespace::TestClass') }

      # This is to keep the previous behavior.
      it 'returns an object' do
        expect(instance).to be_a(described_class)
      end
    end

    context 'when noteable_* arguments are passed' do
      let(:instance) do
        described_class.new(
          'namespace::TestClass',
          noteable_object_kind: 'merge_request',
          noteable_resource_iid: '1234',
          noteable_project_id: '5678'
        )
      end

      it 'uses the noteable_* fields' do
        expect(instance.send(:object_kind)).to eq('merge_request')
        expect(instance.send(:resource_iid)).to eq('1234')
        expect(instance.send(:project_id)).to eq('5678')
      end
    end

    context 'when noteable_* arguments are not passed' do
      let(:instance) { described_class.new('namespace::TestClass', event) }

      it 'uses the event fields' do
        expect(instance.send(:object_kind)).to eq(event.object_kind)
        expect(instance.send(:resource_iid)).to eq(event.iid)
        expect(instance.send(:project_id)).to eq(event.project_id)
      end
    end
  end

  describe 'NOTES_PER_PAGE' do
    it { expect(described_class::NOTES_PER_PAGE).to eq(100) }
  end

  describe '#wrap' do
    it { expect(subject.wrap("Hello World!")).to eq("<!-- triage-serverless TestClass -->\nHello World!") }
  end

  describe '#no_previous_comment?' do
    %w[Issue MergeRequest].each do |resource_type|
      context "with a #{resource_type} event" do
        include_context 'with event', Triage.const_get("#{resource_type}Event", false) do
          let(:event_attrs) do
            {
              object_kind: resource_type.gsub(/(?<lowercase_letter>[a-z])(?<uppercase_letter>[A-Z])/, '\k<lowercase_letter>_\k<uppercase_letter>').downcase,
              issue?: resource_type == "Issue",
              merge_request?: resource_type == "MergeRequest",
              project_id: 42,
              iid: 12
            }
          end
        end

        context "when there is no previous comment" do
          it "returns true" do
            expect_notes_request_with(event, body: nil) do
              expect(subject.no_previous_comment?).to be(true)
            end
          end
        end

        context "when there is a previous comment" do
          let(:note) do
            "<!-- triage-serverless TestClass -->\nHello World!"
          end

          it "returns false" do
            expect_notes_request_with(event, body: note) do
              expect(subject.no_previous_comment?).to be(false)
            end
          end

          context "when the marker is not at the beginning of the line" do
            it "returns false" do
              expect_notes_request_with(event, body: "There can be another marker at the beginning!\n#{note}") do
                expect(subject.no_previous_comment?).to be(false)
              end
            end
          end

          context "when we specify who this comment should be from" do
            subject do
              described_class.new('namespace::TestClass', event, from: expected_author)
            end

            context "when it is from the expected user" do
              let(:expected_author) { note_author }

              it "returns false" do
                expect_notes_request_with(event, body: note) do
                  expect(subject.no_previous_comment?).to be(false)
                end
              end
            end

            context "when it is not from the expected user" do
              let(:expected_author) { note_author.swapcase }

              it "returns true" do
                expect_notes_request_with(event, body: note) do
                  expect(subject.no_previous_comment?).to be(true)
                end
              end
            end

            context "when we specific a unique comment identifier different from a previous comment" do
              subject do
                described_class.new('namespace::TestClass', event, 'string 1234, not TestClass!')
              end

              it "returns true" do
                expect_notes_request_with(event, body: note) do
                  expect(subject.no_previous_comment?).to be(true)
                end
              end
            end

            context "when we specify a unique comment identifier same as a previous comment" do
              subject do
                described_class.new('namespace::TestClass', event, 'TestClass')
              end

              it "returns false" do
                expect_notes_request_with(event, body: note) do
                  expect(subject.no_previous_comment?).to be(false)
                end
              end
            end
          end
        end
      end
    end
  end

  describe '#previous_comment' do
    %w[Issue MergeRequest].each do |resource_type|
      context "with a #{resource_type} event" do
        include_context 'with event', Triage.const_get("#{resource_type}Event", false) do
          let(:event_attrs) do
            {
              object_kind: resource_type.gsub(/(?<lowercase_letter>[a-z])(?<uppercase_letter>[A-Z])/, '\k<lowercase_letter>_\k<uppercase_letter>').downcase,
              issue?: resource_type == "Issue",
              merge_request?: resource_type == "MergeRequest",
              project_id: 42,
              iid: 12
            }
          end
        end

        context "when a previous comment is found" do
          expected_body = '<!-- triage-serverless TestClass --> a comment'
          it "returns the comment" do
            expect_notes_request_with(event, body: expected_body) do
              expect(subject.previous_comment.body).to eq(expected_body)
            end
          end
        end

        context "when a previous comment is not found" do
          it "returns nil" do
            expect_notes_request_with(event, body: 'a comment') do
              expect(subject.previous_comment).to be_nil
            end
          end
        end
      end
    end
  end

  describe '#previous_discussion' do
    let(:note) { instance_double('comment', body: '<!-- triage-serverless TestClass --> a comment') }
    let(:discussion) { { 'notes' => [note] } }

    before do
      allow(subject).to receive(:resource_discussions).and_return([discussion])
    end

    it 'returns a note that matches the hidden comment' do
      expect(subject.previous_discussion).to eq(discussion)
    end
  end

  describe '#previous_discussion_comment' do
    let(:note) { instance_double('comment', body: '<!-- triage-serverless TestClass --> a comment') }
    let(:discussion) { { 'notes' => [note] } }

    before do
      allow(subject).to receive(:resource_discussions).and_return([discussion])
    end

    context 'when a previous discussion is found' do
      it 'returns the discussion comment' do
        expect(subject.previous_discussion_comment.body).to eq('<!-- triage-serverless TestClass --> a comment')
      end
    end

    context 'when a previous discussion is not found' do
      let(:note) { instance_double('comment', body: 'a note') }
      let(:discussion) { { 'notes' => [note] } }

      it 'returns nil' do
        expect(subject.previous_discussion_comment).to be_nil
      end
    end
  end

  describe '#delete_previous_comment' do
    let(:note) do
      "<!-- triage-serverless TestClass -->"
    end

    context 'when previous_comment is not found' do
      before do
        allow(subject).to receive(:previous_comment).and_return(nil)
      end

      it 'does not delete previous comment' do
        expect(Triage.api_client).not_to receive(:delete)

        subject.delete_previous_comment
      end
    end

    context 'when previous_comment is found' do
      let(:notes_id) { 9 }
      let(:expected_path) { "/projects/#{event.project_id}/issues/#{event.iid}/notes/#{notes_id}" }

      before do
        allow(subject).to receive(:previous_comment).and_return(
          instance_double("comment", id: notes_id)
        )
      end

      it 'deletes previous comment' do
        expect(Triage.api_client).to receive(:delete).with(expected_path)
        expect(subject.delete_previous_comment).to eq("#{expected_path} deleted!")
      end

      context 'when the api call failed' do
        before do
          allow(Triage.api_client).to receive(:delete).with(expected_path).and_raise('some error')
        end

        it 'returns an error message' do
          expect(subject.delete_previous_comment).to eq(
            "Failed to delete #{expected_path}: some error"
          )
        end
      end
    end
  end
end
