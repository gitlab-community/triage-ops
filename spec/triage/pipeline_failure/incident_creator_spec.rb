# frozen_string_literal: true

require 'spec_helper'

require_relative '../../../triage/triage/event'
require_relative '../../../triage/triage/pipeline_failure/incident_creator'
require_relative '../../../triage/triage/pipeline_failure/config/master_branch'
require_relative '../../../triage/triage/pipeline_failure/config/dev_master_branch'
require_relative '../../../triage/triage/pipeline_failure/config/stable_branch'

RSpec.describe Triage::PipelineFailure::IncidentCreator do
  let(:event) do
    instance_double(Triage::PipelineEvent,
      id: 42,
      project_path_with_namespace: 'gitlab-org/gitlab',
      project_id: 999,
      ref: 'master',
      sha: 'sha',
      source_job_id: nil,
      web_url: 'web_url',
      commit_header: 'commit_header',
      event_actor: Triage::User.new(id: '1', name: 'John Doe', username: 'john'),
      source: 'source',
      created_at: Time.now,
      merge_request: Triage::MergeRequest.new(title: 'Hello', author: { id: '1' }),
      merge_request_pipeline?: false,
      project_web_url: "https://gitlab.test/gitlab-org/gitlab",
      instance: :com)
  end

  let(:second_job_name) { 'bar' }

  let(:incident_double) do
    double('Incident',
      project_id: config.incident_project_id,
      iid: 4567,
      web_url: 'incident_web_url'
    )
  end

  let(:root_cause_analysis_comment) { nil }
  let(:investigation_comment) { nil }
  let(:duplicate) { false }
  let(:duplicate_source_incident) { nil }
  let(:post_duplication_escalation_warning) { false }

  let(:duplicate_incident_manager_double) do
    instance_double(Triage::PipelineFailure::DuplicateIncidentManager,
      has_duplicate?: duplicate,
      post_warning?: post_duplication_escalation_warning,
      duplicate_source_incident: duplicate_source_incident,
      warning_comment_body: 'escalation_warning'
    )
  end

  let(:triager) do
    instance_double(Triage::PipelineFailure::TriageIncident,
      attribution_comment: '',
      root_cause_analysis_comment: root_cause_analysis_comment,
      investigation_comment: investigation_comment,
      incident_labels: triager_incident_labels,
      duration_analysis_comment: nil,
      duplicate_incident_manager: duplicate_incident_manager_double,
      duplicate?: duplicate)
  end

  let(:triager_incident_labels) do
    [*config.incident_labels, 'group_label', 'root_cause_label', 'test-level-label']
  end

  let(:config) { Triage::PipelineFailure::Config::MasterBranch.new(event) }

  subject(:incident_creator) do
    described_class.new(
      event: event,
      config: Triage::PipelineFailure::Config::MasterBranch.new(event),
      failed_jobs: [
        double(id: '1', name: 'foo', web_url: 'foo_web_url'),
        double(id: '2', name: second_job_name, web_url: 'foo_web_url')
      ]
    )
  end

  around do |example|
    Timecop.freeze(Time.utc(2020, 3, 31, 8)) { example.run }
  end

  before do
    allow(Triage::PipelineFailure::TriageIncident).to receive(:new).and_return(triager)
    allow(Triage.api_client).to receive(:issue).with(config.incident_project_id, 4567)
  end

  describe '#execute' do
    shared_examples 'incident creation' do
      let(:discussion_path) { "/projects/#{config.incident_project_id}/issues/#{incident_double.iid}/discussions" }
      let(:job_trace) { 'ERROR: Job failed: failed to pull image "registry.gitlab.com"' }

      context 'when ref is master' do
        context 'with job trace showing RSpec test failures' do
          let(:root_cause_analysis_comment) { "\n\nroot_cause_analysis_comment" }
          let(:investigation_comment) { "\n\ninvestigation_comment" }

          it 'creates an incident, attributes to a group, and posts relevant job trace to investigation steps' do
            expect(Triage.api_client).to receive(:create_issue)
              .with(
                config.incident_project_id,
                expected_incident_title,
                {
                  issue_type: 'incident',
                  description: format(config.incident_template, incident_creator.__send__(:template_variables)),
                  labels: triager_incident_labels
                })
              .and_return(incident_double)

            expect(Triage.api_client).to receive(:post)
              .with(discussion_path, body: { body: "## Root Cause Analysis\n\nroot_cause_analysis_comment" })

            expect(Triage.api_client).to receive(:post)
              .with(discussion_path, body: { body: "## Investigation Steps\n\ninvestigation_comment" })

            expect(Triage.api_client).to receive(:post)
              .with(discussion_path, body: { body: '## Duration Analysis' })

            incident_creator.execute
          end
        end
      end
    end

    it_behaves_like 'incident creation' do
      let(:expected_incident_title) { "Tuesday 2020-03-31 08:00 UTC - `gitlab-org/gitlab` broken `master` with foo, #{second_job_name}" }
    end

    context 'with a long job name' do
      let(:second_job_name) { 'a' * 180 }

      it_behaves_like 'incident creation' do
        let(:expected_incident_title) { "Tuesday 2020-03-31 08:00 UTC - `gitlab-org/gitlab` broken `master` with foo, #{'a' * 175}..." }
      end
    end

    context 'when the previous incident failed the same jobs and was closed as a duplicate of an earlier incident' do
      let(:duplicate) { true }
      let(:duplicate_source_incident_iid) { 15 }
      let(:root_cause_analysis_comment) { "\n\nroot_cause_analysis_comment" }
      let(:discussion_path) { "/projects/#{config.incident_project_id}/issues/#{incident_double.iid}/discussions" }
      let(:expected_incident_title) { "Tuesday 2020-03-31 08:00 UTC - `gitlab-org/gitlab` broken `master` with foo, bar" }

      it 'creates an incident and marks as duplicate' do
        expect(Triage.api_client).to receive(:create_issue)
          .with(
            config.incident_project_id,
            expected_incident_title,
            {
              issue_type: 'incident',
              description: format(config.incident_template, incident_creator.__send__(:template_variables)),
              labels: triager_incident_labels
            })
          .and_return(incident_double)

        expect(Triage.api_client).to receive(:post)
          .with(discussion_path, body: { body: "## Root Cause Analysis\n\nroot_cause_analysis_comment" })

        expect(Triage.api_client).to receive(:post)
          .with(discussion_path, body: { body: "## Investigation Steps" })

        expect(Triage.api_client).to receive(:post)
          .with(discussion_path, body: { body: '## Duration Analysis' })

        incident_creator.execute
      end

      context 'when posting duplicate and escalation warning' do
        let(:post_duplication_escalation_warning) { true }
        let(:duplicate_source_incident) do
          double('Incident',
            project_id: config.incident_project_id,
            iid: duplicate_source_incident_iid,
            web_url: 'incident_web_url'
          )
        end

        it 'creates an incident, marks as duplicate, and updates the duplicate source incident with an escalation warning' do
          expect(Triage.api_client).to receive(:create_issue)
          .with(
            config.incident_project_id,
            expected_incident_title,
            {
              issue_type: 'incident',
              description: format(config.incident_template, incident_creator.__send__(:template_variables)),
              labels: triager_incident_labels
            })
          .and_return(incident_double)

          expect(Triage.api_client).to receive(:post)
            .with(discussion_path, body: { body: "## Root Cause Analysis\n\nroot_cause_analysis_comment" })

          expect(Triage.api_client).to receive(:post)
            .with(discussion_path, body: { body: "## Investigation Steps" })

          expect(Triage.api_client).to receive(:post)
            .with(discussion_path, body: { body: '## Duration Analysis' })

          expect(Triage.api_client).to receive(:post)
            .with(
              "/projects/#{config.incident_project_id}/issues/#{duplicate_source_incident_iid}/discussions",
              body: { body: 'escalation_warning' }
            )

          incident_creator.execute
        end
      end
    end
  end
end
