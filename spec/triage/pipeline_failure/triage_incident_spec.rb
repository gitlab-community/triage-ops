# frozen_string_literal: true

require 'spec_helper'
require 'tempfile'

require_relative '../../../triage/triage/event'
require_relative '../../../triage/triage/pipeline_failure/triage_incident'
require_relative '../../../triage/triage/pipeline_failure/config/master_branch'

RSpec.describe Triage::PipelineFailure::TriageIncident do
  def stub_ci_job(job_id: 999, **attrs)
    name = "job #{job_id}"
    web_url = "https://gitlab.com/group/project/-/jobs/#{job_id}"
    default_attrs = {
      job_id: job_id,
      name: "job #{job_id}",
      web_url: web_url,
      markdown_link: "[#{name}](#{web_url})",
      failure_root_cause_label: 'master-broken::undetermined',
      test_failure_summary_markdown: '',
      attribution_message_markdown: '',
      rspec_run_time_summary_markdown: '',
      potential_responsible_group_labels: []
    }
    instance_double(Triage::CiJob, default_attrs.merge(attrs))
  end

  def duplicate_jobs(template_job, count, **attrs)
    (1..count).map do
      stub_ci_job(name: template_job.name, web_url: template_job.web_url, instance: :dev, **attrs)
    end
  end

  let(:job_failed_to_pull_image) { stub_ci_job(failure_root_cause_label: 'master-broken::infrastructure::failed-to-pull-image') }
  let(:job_infrastructure) { stub_ci_job(failure_root_cause_label: 'master-broken::infrastructure') }
  let(:job_gitlab_com_overloaded) { stub_ci_job(failure_root_cause_label: 'master-broken::gitlab-com-overloaded') }
  let(:job_undetermined) { stub_ci_job(failure_root_cause_label: 'master-broken::undetermined') }
  let(:job_runner_disk_full) { stub_ci_job(failure_root_cause_label: 'master-broken::infrastructure::runner-disk-full') }
  let(:job_timeout) { stub_ci_job(failure_root_cause_label: 'master-broken::job-timeout') }
  let(:job_rspec) { stub_ci_job(potential_responsible_group_labels: ['group::foundations', 'group::knowledge']) }
  let(:job_jest) { stub_ci_job(potential_responsible_group_labels: ['frontend']) }

  let(:event) do
    instance_double(Triage::PipelineEvent,
      id: 123,
      project_id:
      Triage::Event::GITLAB_PROJECT_ID, instance: :com,
      project_path_with_namespace: 'gitlab-org/gitlab'
    )
  end

  let(:config) { Triage::PipelineFailure::Config::MasterBranch.new(event) }
  let(:ci_jobs) { [] }

  subject(:triager) { described_class.new(event: event, config: config, ci_jobs: ci_jobs) }

  describe '#top_root_cause_label' do
    context 'with no failed jobs' do
      it 'returns master-broken::undetermined' do
        expect(triager.send(:top_root_cause_label)).to eq('master-broken::undetermined')
      end
    end

    context 'with only 1 job failure which is caused by unknown reason' do
      let(:ci_jobs) { [job_undetermined] }

      it 'returns master-broken::undetermined' do
        expect(triager.send(:top_root_cause_label)).to eq('master-broken::undetermined')
      end
    end

    context 'with only 1 job failure which is caused by runner-disk-full' do
      let(:ci_jobs) { [job_runner_disk_full] }

      it 'returns master-broken::infrastructure::runner-disk-full' do
        expect(triager.send(:top_root_cause_label)).to eq('master-broken::infrastructure::runner-disk-full')
      end
    end

    context 'with mulitple jobs failed due to failed-to-pull-image' do
      let(:ci_jobs) { duplicate_jobs(job_failed_to_pull_image, 10, failure_root_cause_label: 'master-broken::infrastructure::failed-to-pull-image') }

      it 'returns master-broken::infrastructure::failed-to-pull-image' do
        expect(triager.send(:top_root_cause_label)).to eq('master-broken::infrastructure::failed-to-pull-image')
      end
    end

    context 'with most jobs failed due to failed-to-pull-image except 1 unknown root cause' do
      let(:ci_jobs) { duplicate_jobs(job_failed_to_pull_image, 9, failure_root_cause_label: 'master-broken::infrastructure::failed-to-pull-image').push(job_undetermined) }

      it 'returns master-broken::infrastructure::failed-to-pull-image' do
        expect(triager.send(:top_root_cause_label)).to eq('master-broken::infrastructure::failed-to-pull-image')
      end
    end

    context 'with some repeated transient errors and some non-repeated, non-transient errors' do
      let(:ci_jobs) { [job_failed_to_pull_image, job_failed_to_pull_image, job_infrastructure, job_gitlab_com_overloaded, job_undetermined, job_runner_disk_full, job_timeout] }

      it 'return the most repeated root cause label' do
        expect(triager.send(:top_root_cause_label)).to eq('master-broken::infrastructure::failed-to-pull-image')
      end
    end

    context 'when every job has a different root cause' do
      let(:ci_jobs) { [job_failed_to_pull_image, job_infrastructure, job_gitlab_com_overloaded, job_undetermined, job_runner_disk_full, job_timeout] }

      it 'returns any root cause besides master-broken::undetermined' do
        expect(triager.send(:top_root_cause_label)).not_to eq('master-broken::undetermined')
      end
    end
  end

  describe '#top_group_label' do
    context 'with no failed jobs' do
      it 'returns master-broken::undetermined' do
        expect(triager.send(:top_group_label)).to be_nil
      end
    end

    context 'with only 1 job failure which is caused by runner-disk-full' do
      let(:ci_jobs) { [job_runner_disk_full] }

      it 'returns no top_group_label' do
        expect(triager.send(:top_group_label)).to be_nil
      end
    end

    context 'with 1 jest job failure' do
      let(:ci_jobs) { [job_jest] }

      it 'returns frontend as top group label' do
        expect(triager.send(:top_group_label)).to eq('frontend')
      end
    end

    context 'with 1 jest job and 1 timeout job failure' do
      let(:ci_jobs) { [job_timeout, job_jest] }

      it 'returns the attributed group label based on the rspec job failure' do
        expect(triager.send(:top_group_label)).to eq('frontend')
      end
    end

    context 'with only 1 RSpec job failure' do
      let(:ci_jobs) { [job_rspec] }

      it 'returns the corresponding group label' do
        expect(triager.send(:top_group_label)).to eq('group::foundations')
      end
    end

    context 'with 2 RSpec job failure' do
      let(:ci_jobs) { [job_rspec, job_rspec] }

      it 'returns the corresponding group label' do
        expect(triager.send(:top_group_label)).to eq('group::foundations')
      end
    end
  end

  describe '#incident_labels' do
    context 'when failed test not attributed to any group and couldnt identify root cause' do
      let(:failed_unit_test_job) { stub_ci_job(name: 'rspec-ee unit pg14 1/28') }
      let(:ci_jobs) { [failed_unit_test_job] }

      it 'returns default group and undetermined root cause labels' do
        expect(triager.incident_labels).to eq(
          ["master:broken", "master-broken::undetermined", "test-level:unit"]
        )
      end
    end

    context 'when failed test attributed to a group' do
      let(:ci_jobs) { [job_rspec] }

      it 'returns the corresponding group label' do
        expect(triager.incident_labels).to eq(
          ["master:broken", "master-broken::undetermined", "group::foundations"]
        )
      end
    end

    context 'when failed test has a known root cause' do
      let(:ci_jobs) { [job_infrastructure] }

      it 'returns the root cause label and default group label' do
        expect(triager.incident_labels).to eq(
          ["master:broken", "master-broken::infrastructure"]
        )
      end
    end
  end

  describe '#test_level_labels' do
    context 'with no failed jobs' do
      let(:ci_jobs) { [] }

      it 'returns an empty array' do
        expect(triager.send(:test_level_labels)).to eq([])
      end
    end

    context 'with only jest failed job' do
      let(:failed_jest_job) { stub_ci_job(name: 'jest', failure_root_cause_label: 'master-broken::undetermined') }
      let(:ci_jobs) { [failed_jest_job] }

      it 'returns an empty array' do
        expect(triager.send(:test_level_labels)).to eq([])
      end
    end

    context 'with failed jobs include rspec-ee unit pg14 1/28' do
      let(:failed_unit_test_job) { stub_ci_job(name: 'rspec-ee unit pg14 1/28') }
      let(:ci_jobs) { [failed_unit_test_job] }

      it 'returns test-level:unit' do
        expect(triager.send(:test_level_labels)).to eq(['test-level:unit'])
      end
    end

    context 'with failed jobs include all of unit, system, integration, migration and e2e tests ' do
      let(:failed_unit_test_job) { stub_ci_job(name: 'rspec-ee unit pg14 1/28') }
      let(:failed_integration_test_job) { stub_ci_job(name: 'rspec-ee integration pg14 1/28') }
      let(:failed_system_test_job) { stub_ci_job(name: 'rspec-ee system pg14 1/28') }
      let(:failed_migration_test_job) { stub_ci_job(name: 'rspec migration pg14 3/15') }
      let(:failed_e2e_test_job) { stub_ci_job(name: 'e2e:test-on-gdk') }
      let(:ci_jobs) do
        [
          failed_unit_test_job,
          failed_integration_test_job,
          failed_system_test_job,
          failed_migration_test_job,
          failed_e2e_test_job
        ]
      end

      it 'returns all test level labels' do
        expect(triager.send(:test_level_labels)).to match_array(
          [
            'test-level:unit',
            'test-level:system',
            'test-level:integration',
            'test-level:migration',
            'test-level:e2e'
          ]
        )
      end
    end
  end

  describe '#root_cause_analysis_comment' do
    let(:previous_incidents) { [] }

    before do
      allow(Triage.api_client).to receive(:issues).and_return(previous_incidents)
    end

    context 'with all job failures due to a known transient error' do
      let(:ci_jobs) do
        duplicate_jobs(job_failed_to_pull_image,
          10,
          failure_root_cause_label: 'master-broken::infrastructure::failed-to-pull-image'
        )
      end

      it 'determines root cause from trace, retries pipeline, and closes incident' do
        expect(Triage.api_client).to receive(:retry_pipeline).with(
          event.project_id, event.id
        ).and_return(Gitlab::ObjectifiedHash.new({ 'web_url' => 'retried_pipeline_web_url' }))

        expect(triager.root_cause_analysis_comment).to eq(
          <<~MARKDOWN.chomp.prepend("\n\n")
            - [job 999](https://gitlab.com/group/project/-/jobs/999): ~"master-broken::infrastructure::failed-to-pull-image".
            - [job 999](https://gitlab.com/group/project/-/jobs/999): ~"master-broken::infrastructure::failed-to-pull-image".
            - [job 999](https://gitlab.com/group/project/-/jobs/999): ~"master-broken::infrastructure::failed-to-pull-image".
            - [job 999](https://gitlab.com/group/project/-/jobs/999): ~"master-broken::infrastructure::failed-to-pull-image".
            - [job 999](https://gitlab.com/group/project/-/jobs/999): ~"master-broken::infrastructure::failed-to-pull-image".
            - [job 999](https://gitlab.com/group/project/-/jobs/999): ~"master-broken::infrastructure::failed-to-pull-image".
            - [job 999](https://gitlab.com/group/project/-/jobs/999): ~"master-broken::infrastructure::failed-to-pull-image".
            - [job 999](https://gitlab.com/group/project/-/jobs/999): ~"master-broken::infrastructure::failed-to-pull-image".
            - [job 999](https://gitlab.com/group/project/-/jobs/999): ~"master-broken::infrastructure::failed-to-pull-image".
            - [job 999](https://gitlab.com/group/project/-/jobs/999): ~"master-broken::infrastructure::failed-to-pull-image".

            Retried pipeline: retried_pipeline_web_url

            This incident is caused by known transient error(s), closing.

            /close
          MARKDOWN
        )
      end
    end

    context 'with some job failures due to unknown root causes' do
      let(:ci_jobs) do
        duplicate_jobs(job_failed_to_pull_image,
          10,
          failure_root_cause_label: 'master-broken::infrastructure::failed-to-pull-image'
        ).push(job_undetermined)
      end

      it 'labels each job, does not retry pipeline or close incident' do
        expect(Triage.api_client).not_to receive(:retry_pipeline)

        expect(triager.root_cause_analysis_comment).to eq(
          <<~MARKDOWN.chomp.prepend("\n\n")
            - [job 999](https://gitlab.com/group/project/-/jobs/999): ~"master-broken::infrastructure::failed-to-pull-image".
            - [job 999](https://gitlab.com/group/project/-/jobs/999): ~"master-broken::infrastructure::failed-to-pull-image".
            - [job 999](https://gitlab.com/group/project/-/jobs/999): ~"master-broken::infrastructure::failed-to-pull-image".
            - [job 999](https://gitlab.com/group/project/-/jobs/999): ~"master-broken::infrastructure::failed-to-pull-image".
            - [job 999](https://gitlab.com/group/project/-/jobs/999): ~"master-broken::infrastructure::failed-to-pull-image".
            - [job 999](https://gitlab.com/group/project/-/jobs/999): ~"master-broken::infrastructure::failed-to-pull-image".
            - [job 999](https://gitlab.com/group/project/-/jobs/999): ~"master-broken::infrastructure::failed-to-pull-image".
            - [job 999](https://gitlab.com/group/project/-/jobs/999): ~"master-broken::infrastructure::failed-to-pull-image".
            - [job 999](https://gitlab.com/group/project/-/jobs/999): ~"master-broken::infrastructure::failed-to-pull-image".
            - [job 999](https://gitlab.com/group/project/-/jobs/999): ~"master-broken::infrastructure::failed-to-pull-image".
            - [job 999](https://gitlab.com/group/project/-/jobs/999): ~"master-broken::undetermined".
          MARKDOWN
        )
      end
    end

    context 'with less than 10 failed jobs' do
      let(:ci_jobs) do
        duplicate_jobs(job_failed_to_pull_image,
          9,
          failure_root_cause_label: 'master-broken::infrastructure::failed-to-pull-image'
        )
      end

      context 'with no failed job' do
        let(:ci_jobs) { [] }

        context 'when no previous incident' do
          it 'returns nil' do
            expect(triager.root_cause_analysis_comment).to be_nil
          end
        end

        context 'when previous incident also shows empty failed jobs' do
          let(:description_with_empty_jobs) do
            <<~MARKDOWN
              **Failed jobs (0):**

              <!-- job_names: [] This info is hidden used by automation. -->
            MARKDOWN
          end

          let(:previous_incidents) do
            [
              instance_double(
                'Previous Incident',
                title: '`gitlab-org/gitlab` broken `master` with',
                description: description_with_empty_jobs,
                web_url: "https://gitlab.com/group/project/issues/1",
                _links: { self: "https://gitlab.com/api/v4/projects/1/issues/1" }
              )
            ]
          end

          it 'marks the current incident as duplicate' do
            expect(subject.root_cause_analysis_comment).to eq(
              <<~MARKDOWN.chomp.prepend("\n\n")
                Closing as a duplicate of incident https://gitlab.com/group/project/issues/1. Please reopen it if you don't think this is a duplicate.

                /label ~"auto closed" ~"closed::duplicate"
                /duplicate https://gitlab.com/group/project/issues/1
              MARKDOWN
            )
          end
        end
      end

      context 'with 1 failed job due to a known transient error' do
        let(:ci_jobs) { [job_infrastructure] }

        it 'retries the job and closes incident' do
          expect(ci_jobs).to all(receive(:retry).and_return(Gitlab::ObjectifiedHash.new({ 'web_url' => 'retry_job_web_url' })))

          expect(triager.root_cause_analysis_comment).to eq(
            <<~MARKDOWN.chomp.prepend("\n\n")
              - [job 999](https://gitlab.com/group/project/-/jobs/999): ~"master-broken::infrastructure". Retried at: retry_job_web_url

              This incident is caused by known transient error(s), closing.

              /close
            MARKDOWN
          )
        end
      end

      context 'when multiple root causes detected with some repeated causes and some non-transient errors' do
        let(:ci_jobs) { [job_gitlab_com_overloaded, job_undetermined, job_runner_disk_full] }

        it 'only retries each job failed with transient error' do
          expect(job_gitlab_com_overloaded).to receive(:retry).and_return(Gitlab::ObjectifiedHash.new({ 'web_url' => 'retry_job_web_url' }))
          expect(job_runner_disk_full).to receive(:retry).and_return(Gitlab::ObjectifiedHash.new({ 'web_url' => 'retry_job_web_url' }))
          expect(job_undetermined).not_to receive(:retry)

          expect(triager.root_cause_analysis_comment).to eq(
            <<~MARKDOWN.chomp.prepend("\n\n")
              - [job 999](https://gitlab.com/group/project/-/jobs/999): ~"master-broken::gitlab-com-overloaded". Retried at: retry_job_web_url
              - [job 999](https://gitlab.com/group/project/-/jobs/999): ~"master-broken::undetermined".
              - [job 999](https://gitlab.com/group/project/-/jobs/999): ~"master-broken::infrastructure::runner-disk-full". Retried at: retry_job_web_url
            MARKDOWN
          )
        end
      end

      context 'when multiple root causes detected, all caused by transient errors' do
        let(:ci_jobs) { [job_failed_to_pull_image, job_infrastructure, job_gitlab_com_overloaded, job_gitlab_com_overloaded, job_runner_disk_full] }

        it 'labels and retries each job, and closes the incident' do
          expect(ci_jobs).to all(receive(:retry).and_return(Gitlab::ObjectifiedHash.new({ 'web_url' => 'retry_job_web_url' })))

          expect(triager.root_cause_analysis_comment).to eq(
            <<~MARKDOWN.chomp.prepend("\n\n")
              - [job 999](https://gitlab.com/group/project/-/jobs/999): ~"master-broken::infrastructure::failed-to-pull-image". Retried at: retry_job_web_url
              - [job 999](https://gitlab.com/group/project/-/jobs/999): ~"master-broken::infrastructure". Retried at: retry_job_web_url
              - [job 999](https://gitlab.com/group/project/-/jobs/999): ~"master-broken::gitlab-com-overloaded". Retried at: retry_job_web_url
              - [job 999](https://gitlab.com/group/project/-/jobs/999): ~"master-broken::gitlab-com-overloaded". Retried at: retry_job_web_url
              - [job 999](https://gitlab.com/group/project/-/jobs/999): ~"master-broken::infrastructure::runner-disk-full". Retried at: retry_job_web_url

              This incident is caused by known transient error(s), closing.

              /close
            MARKDOWN
          )
        end
      end

      context 'when a workhorse job failed' do
        let(:ci_jobs) { [stub_ci_job(potential_responsible_group_labels: ['group::source code'])] }

        it 'does not retry any job and labels the job master-broken::undermined' do
          expect(triager.root_cause_analysis_comment).to eq(
            <<~MARKDOWN.chomp.prepend("\n\n")
              - [job 999](https://gitlab.com/group/project/-/jobs/999): ~"master-broken::undetermined".
            MARKDOWN
          )
        end
      end

      context 'when none of the job failures are recognized' do
        let(:ci_jobs) { duplicate_jobs(job_undetermined, 4) }

        it 'does not retry any job and labels each job master-broken::undetermined' do
          expect(triager.root_cause_analysis_comment).to eq(
            <<~MARKDOWN.chomp.prepend("\n\n")
              - [job 999](https://gitlab.com/group/project/-/jobs/999): ~"master-broken::undetermined".
              - [job 999](https://gitlab.com/group/project/-/jobs/999): ~"master-broken::undetermined".
              - [job 999](https://gitlab.com/group/project/-/jobs/999): ~"master-broken::undetermined".
              - [job 999](https://gitlab.com/group/project/-/jobs/999): ~"master-broken::undetermined".
            MARKDOWN
          )
        end
      end
    end

    context 'with previous incident failed with the same job closed as duplicate of an earlier incident' do
      let(:ci_jobs) { [job_undetermined] }

      let(:description_with_job_999) do
        <<~MARKDOWN
          **Failed jobs (1):**

          1. [job 999](https://gitlab.com/group/project/-/jobs/999): ~"master-broken::undetermined".
        MARKDOWN
      end

      let(:previous_incidents) do
        [
          instance_double(
            'Previous Incident',
            title: '`gitlab-org/gitlab` broken `master` with job 999',
            web_url: "https://gitlab.com/group/project/issues/1",
            description: description_with_job_999,
            _links: { self: "https://gitlab.com/api/v4/projects/1/issues/1" }
          )
        ]
      end

      it 'marks the current incident as duplicate' do
        expect(subject.root_cause_analysis_comment).to eq(
          <<~MARKDOWN.chomp.prepend("\n\n")
            - [job 999](https://gitlab.com/group/project/-/jobs/999): ~"master-broken::undetermined".

            Closing as a duplicate of incident https://gitlab.com/group/project/issues/1. Please reopen it if you don't think this is a duplicate.

            /label ~"auto closed" ~"closed::duplicate"
            /duplicate https://gitlab.com/group/project/issues/1
          MARKDOWN
        )
      end
    end
  end

  describe '#attribution_comment' do
    context 'with rspec failures' do
      let(:ci_jobs) { [stub_ci_job(potential_responsible_group_labels: ['group::group1'], attribution_message_markdown: '- foo bar')] }

      it 'prints the job id and feature category' do
        expect(subject.attribution_comment).to eq(
          <<~MARKDOWN.chomp.prepend("\n\n")
          - foo bar

          **This incident is attributed to ~"group::group1" and posted in `#master-broken`, `#group1`.**
          MARKDOWN
        )
      end

      context 'when the group is unknown' do
        let(:ci_jobs) { [stub_ci_job(potential_responsible_group_labels: [], attribution_message_markdown: '- foo bar')] }

        it 'only prints where it was posted' do
          expect(subject.attribution_comment).to eq(
            <<~MARKDOWN.chomp.prepend("\n\n")
            - foo bar

            **This incident is unattributed and posted in `#master-broken`.**
            MARKDOWN
          )
        end
      end
    end

    context 'with jest job failure' do
      let(:ci_jobs) { [stub_ci_job(potential_responsible_group_labels: ['frontend'])] }

      it 'attributes to frontend' do
        expect(subject.attribution_comment).to eq(
          <<~MARKDOWN.chomp.prepend("\n\n")
            **This incident is attributed to ~"frontend" and posted in `#master-broken`, `#frontend`.**
          MARKDOWN
        )
      end
    end

    context 'with e2e job failure' do
      let(:ci_jobs) { [stub_ci_job(name: 'e2e:test-on-gdk', potential_responsible_group_labels: 'test-level:e2e')] }

      it 'attributes to infrastructure' do
        expect(subject.attribution_comment).to eq(
          <<~MARKDOWN.chomp.prepend("\n\n")
            **This incident is attributed to ~"test-level:e2e" and posted in `#master-broken`, `#e2e-run-master`.**
          MARKDOWN
        )
      end
    end

    context 'with gitlab.com overloaded error' do
      let(:ci_jobs) { [stub_ci_job(potential_responsible_group_labels: ['group::gitaly'])] }

      it 'attributes to gitaly' do
        expect(subject.attribution_comment).to eq(
          <<~MARKDOWN.chomp.prepend("\n\n")
            **This incident is attributed to ~"group::gitaly" and posted in `#master-broken`, `#gitaly-alerts`.**
          MARKDOWN
        )
      end
    end
  end

  describe '#investigation_comment' do
    before do
      allow(Triage.api_client).to receive(:issues).and_return([])
    end

    context 'with 1 job failed on trasient error without rspec test summary' do
      let(:ci_jobs) { [job_failed_to_pull_image] }

      it 'returns nil' do
        expect(triager.investigation_comment).to be_nil
      end
    end

    context 'with 1 failed job showing rspec failures' do
      let(:ci_jobs) { [stub_ci_job(test_failure_summary_markdown: 'test failure summary')] }

      it 'prints test log' do
        expect(triager.investigation_comment).to eq(
          <<~MARKDOWN.chomp.prepend("\n\n")
          test failure summary
          MARKDOWN
        )
      end
    end

    context 'with 1 failed job linking to a flaky rspec test' do
      let(:flaky_test_job) do
        stub_ci_job(
          failure_root_cause_label: 'master-broken::flaky-test',
          test_failure_summary_markdown: "test failure summary\n/duplicate #1234"
        )
      end

      let(:ci_jobs) { [flaky_test_job] }

      it 'prints test log' do
        expect(triager.investigation_comment).to eq(
          <<~MARKDOWN.chomp.prepend("\n\n")
          test failure summary
          /duplicate #1234
          MARKDOWN
        )
      end
    end
  end

  describe '#duration_analysis_comment' do
    let(:ci_jobs) do
      [
        stub_ci_job(rspec_run_time_summary_markdown: 'rspec_run_time_summary_markdown'),
        stub_ci_job(rspec_run_time_summary_markdown: 'another_rspec_run_time_summary_markdown'),
        stub_ci_job(rspec_run_time_summary_markdown: nil)
      ]
    end

    context 'when duration analysis comment does not exceed the RSPEC_DURATION_COMMENT_LIMIT' do
      it 'returns duration data for the job' do
        expect(triager.duration_analysis_comment).to eq(
          <<~MARKDOWN.chomp.prepend("\n\n")
          rspec_run_time_summary_markdown

          another_rspec_run_time_summary_markdown
          MARKDOWN
        )
      end
    end

    context 'when rspec_duration_comment_limit only allows showing duration data for 1 of the 3 jobs' do
      it 'returns duration data within the size limit' do
        allow(triager).to receive(:rspec_duration_comment_limit).and_return(50)

        expect(triager.duration_analysis_comment).to eq(
          <<~MARKDOWN.chomp.prepend("\n\n")
          rspec_run_time_summary_markdown
          MARKDOWN
        )
      end
    end

    context 'when rspec_duration_comment_limit is too small to show any duration analysis comment' do
      it 'does not return duration data for any job' do
        allow(triager).to receive(:rspec_duration_comment_limit).and_return(10)

        expect(triager.duration_analysis_comment).to eq('')
      end
    end
  end

  describe '#duplicate?' do
    let(:canonical_incident_triggered_by_same_commit) { false }
    let(:has_duplicate) { false }

    let(:duplicate_incident_manager) do
      instance_double(
        Triage::PipelineFailure::DuplicateIncidentManager,
        canonical_incident_triggered_by_same_commit?: canonical_incident_triggered_by_same_commit,
        has_duplicate?: has_duplicate
      )
    end

    before do
      allow(Triage::PipelineFailure::DuplicateIncidentManager).to receive(:new).and_return(duplicate_incident_manager)
    end

    context 'when incident is not a duplicate' do
      it 'returns false' do
        expect(triager.duplicate?).to be false
      end
    end

    context 'when incident is from a canonical stable branch' do
      context 'when there exists a duplicate incident' do
        let(:has_duplicate) { true }

        it 'returns true' do
          expect(triager.duplicate?).to be true
        end
      end
    end

    context 'when incident is from a security stable branch' do
      let(:config) { Triage::PipelineFailure::Config::SecurityStableBranch.new(event) }

      context 'when there exists a canonical incident triggered by the same commit' do
        let(:canonical_incident_triggered_by_same_commit) { true }

        it 'returns true' do
          expect(triager.duplicate?).to be true
        end
      end
    end
  end
end
