# frozen_string_literal: true

require 'spec_helper'
require_relative '../../lib/devops_labels'

RSpec.describe DevopsLabels do
  describe '.category_labels' do
    it 'returns the category labels' do
      labels = described_class.category_labels

      expect(labels).to include('Category:1', 'feature1', 'Category:category_with_no_group', 'feature6')
    end
  end

  describe '.group_category_labels_per_category' do
    subject { described_class.group_category_labels_per_category(category_key) }

    context 'when category is invalid' do
      let(:category_key) { 'invalid_category' }

      it 'returns nil' do
        expect(subject).to be_nil
      end
    end

    context 'when category is valid but is not matched to a group' do
      let(:category_key) { 'category_with_no_group' }

      it 'returns nil' do
        expect(subject).to be_nil
      end
    end

    context 'when category is valid and matched to a group' do
      let(:category_key) { 'category5' }

      it 'returns group data' do
        expect(subject).to eq(
          { 'category_label' => 'Category:5',
            'group_label' => 'group::group3' }
        )
      end
    end
  end

  describe '.group_for_user' do
    where(:username, :expected_group) do
      [
        ['p-1', nil],
        %w[p-2 group1],
        %w[p-3 group1],
        ['non-gitlab-user', nil]
      ]
    end

    with_them do
      it 'returns teams of user' do
        expect(described_class.group_for_user(username)&.key).to eq(expected_group)
      end
    end
  end

  describe DevopsLabels::Context do
    let(:resource_klass) do
      Struct.new(:labels) do
        include DevopsLabels::Context
      end
    end

    let(:label_klass) do
      Struct.new(:name)
    end

    let(:resource) { resource_klass.new([]) }

    describe '#label_names' do
      it 'returns [] if the resource has no label' do
        resource = resource_klass.new([])

        expect(resource.label_names).to eq([])
      end

      it 'returns the label names if the resource has labels' do
        resource = resource_klass.new([label_klass.new(Hierarchy::Stage.all_labels.first)])

        expect(resource.label_names).to eq([Hierarchy::Stage.all_labels.first])
      end
    end

    describe '#current_section_label' do
      it 'returns nil if the resource has no section label' do
        resource = resource_klass.new([])

        expect(resource.current_section_label).to be_nil
      end

      it 'returns the section label if the resource has a section label' do
        resource = resource_klass.new([label_klass.new(Hierarchy::Section.all_labels.first)])

        expect(resource.current_section_label).to eq(Hierarchy::Section.all_labels.first)
      end
    end

    describe '#current_stage_label' do
      it 'returns nil if the resource has no stage label' do
        resource = resource_klass.new([])

        expect(resource.current_stage_label).to be_nil
      end

      it 'returns the stage label if the resource has a stage label' do
        resource = resource_klass.new([label_klass.new(Hierarchy::Stage.all_labels.first)])

        expect(resource.current_stage_label).to eq(Hierarchy::Stage.all_labels.first)
      end
    end

    describe '#current_group_label' do
      it 'returns nil if the resource has no group label' do
        resource = resource_klass.new([])

        expect(resource.current_group_label).to be_nil
      end

      it 'returns the group label if the resource has a group label' do
        group_label = "group::group1"
        resource = resource_klass.new([label_klass.new(group_label)])

        expect(resource.current_group_label).to eq(group_label)
      end
    end

    describe '#current_infrastructure_team_label' do
      it 'returns nil if the resource has no infrastructure team label' do
        resource = resource_klass.new([])

        expect(resource.current_infrastructure_team_label).to be_nil
      end

      it 'returns the group label if the resource has an infrastructure team label' do
        resource = resource_klass.new([label_klass.new('team::Scalability')])

        expect(resource.current_infrastructure_team_label).to eq('team::Scalability')
      end
    end

    describe '#current_category_labels' do
      it 'returns [] if the resource has no category label' do
        resource = resource_klass.new([])

        expect(resource.current_category_labels).to eq([])
      end

      it 'returns the category labels if the resource has a category label' do
        resource = resource_klass.new([label_klass.new(DevopsLabels.category_labels.first)])

        expect(resource.current_category_labels).to eq([DevopsLabels.category_labels.first])
      end
    end

    describe '#current_workflow_label' do
      it 'returns nil if the resource has no workflow label' do
        resource = resource_klass.new([])

        expect(resource.current_workflow_label).to be_nil
      end

      it 'returns the workflow label if the resource has a workflow label' do
        resource = resource_klass.new([label_klass.new('workflow::verification')])

        expect(resource.current_workflow_label).to eq('workflow::verification')
      end
    end

    describe '#current_stage_key' do
      it 'returns nil if the resource has no stage label' do
        resource = resource_klass.new([])

        expect(resource.current_stage_key).to be_nil
      end
    end

    describe '#current_group_key' do
      it 'returns nil if the resource has no group label' do
        resource = resource_klass.new([])

        expect(resource.current_group_key).to be_nil
      end

      it 'returns the group name if the resource has a group label' do
        resource = resource_klass.new([label_klass.new("group::group1")])

        expect(resource.current_group_key).to eq(Hierarchy::Group.all.first.key)
      end
    end

    describe '#all_category_labels_for_current_stage' do
      it 'returns [] if the resource has no stage label' do
        resource = resource_klass.new([])

        expect(resource.all_category_labels_for_current_stage).to eq([])
      end

      it 'returns the stage category labels when given a stage name' do
        resource = resource_klass.new([label_klass.new(Hierarchy::Stage.all_labels.first)])

        expect(resource.all_category_labels_for_current_stage).to eq(%w[Category:1 feature1 Category:2 feature2 Category:3 feature3 Category:4 feature4])
      end
    end

    describe '#all_category_labels_for_group' do
      it 'returns [] if the group is nil' do
        expect(resource.all_category_labels_for_current_stage).to eq([])
      end

      it 'returns the group category labels when given a group name' do
        resource = resource_klass.new([])

        expect(resource.all_category_labels_for_group(Hierarchy::Group.new('group1'))).to eq(%w[Category:1 feature1 Category:2 feature2])
      end
    end

    describe '#section_for_stage' do
      it 'returns the section for the specific stage' do
        resource = resource_klass.new([])

        expect(resource.section_for_stage('stage_with_two_groups').key).to eq('section1')
      end
    end

    describe '#section_for_group' do
      it 'returns the section for the specific group' do
        resource = resource_klass.new([])

        expect(resource.section_for_group('group1').key).to eq('section1')
      end
    end

    describe '#has_stage_label?' do
      it 'returns false if the resource has no stage label' do
        resource = resource_klass.new([])

        expect(resource).not_to be_has_stage_label
      end

      it 'returns true if the resource has a stage label' do
        resource = resource_klass.new([label_klass.new(Hierarchy::Stage.all_labels.first)])

        expect(resource).to be_has_stage_label
      end
    end

    describe '#has_group_label?' do
      it 'returns false if the resource has no group label' do
        resource = resource_klass.new([])

        expect(resource).not_to be_has_group_label
      end

      it 'returns false if the resource is labeled with group::not_owned [DEPRECATED]' do
        resource = resource_klass.new([label_klass.new('group::not_owned [DEPRECATED]')])

        expect(resource).not_to have_group_label
      end

      it 'returns true if the resource has a group label' do
        resource = resource_klass.new([label_klass.new("group::group1")])

        expect(resource).to have_group_label
      end
    end

    describe '#has_specialization_label?' do
      it 'returns false if the resource has no specialization label' do
        resource = resource_klass.new([])

        expect(resource).not_to be_has_specialization_label
      end

      it 'returns true if the resource has a specialization label' do
        resource = resource_klass.new([label_klass.new("backend")])

        expect(resource).to have_specialization_label
      end

      it 'returns true if the resource has multiple specialization labels' do
        resource = resource_klass.new([label_klass.new("backend"), label_klass.new("frontend")])

        expect(resource).to have_specialization_label
      end
    end

    describe "workflow_label_is?" do
      it 'returns true if exact workflow label is matched' do
        resource = resource_klass.new([label_klass.new("workflow::foo")])

        expect(resource.workflow_label_is?('workflow::foo')).to be true
      end

      it 'returns true if one of many workflow labels match' do
        resource = resource_klass.new([label_klass.new("workflow::foo")])

        expect(resource.workflow_label_is?(%w[workflow::foo workflow::bar])).to be true
      end

      it 'returns false if no workflow label is matched' do
        resource = resource_klass.new([label_klass.new("workflow::foo")])

        expect(resource.workflow_label_is?('workflow::bar')).to be false
      end
    end

    describe '#can_add_default_group?' do
      it 'returns true if the resource has no labels' do
        resource = resource_klass.new([])

        expect(resource).to be_can_add_default_group
      end

      it 'returns false if the resource has a group label' do
        resource = resource_klass.new([label_klass.new('group::group1')])

        expect(resource).not_to be_can_add_default_group
      end

      it 'returns false if the resource has an infrastructure team label' do
        resource = resource_klass.new([label_klass.new('team::Scalability')])

        expect(resource).not_to be_can_add_default_group
      end

      it 'returns true if the resource has a stage label' do
        resource = resource_klass.new([label_klass.new('devops::manage')])

        expect(resource).to be_can_add_default_group
      end
    end

    describe '#can_add_default_group_and_stage?' do
      it 'returns true if the resource has no labels' do
        resource = resource_klass.new([])

        expect(resource).to be_can_add_default_group_and_stage
      end

      it 'returns false if the resource has a group label' do
        resource = resource_klass.new([label_klass.new('group::group1')])

        expect(resource).not_to be_can_add_default_group_and_stage
      end

      it 'returns false if the resource has an infrastructure team label' do
        resource = resource_klass.new([label_klass.new('team::Scalability')])

        expect(resource).not_to be_can_add_default_group_and_stage
      end

      it 'returns false if the resource has a stage label' do
        resource = resource_klass.new([label_klass.new('devops::stage with two groups')])

        expect(resource).not_to be_can_add_default_group_and_stage
      end
    end

    describe '#has_category_label?' do
      it 'returns false if the resource has no category label' do
        resource = resource_klass.new([])

        expect(resource).not_to have_category_label
      end

      it 'returns true if the resource has a category label' do
        resource = resource_klass.new([label_klass.new(DevopsLabels.category_labels.first)])

        expect(resource).to have_category_label
      end
    end

    describe '#has_category_label_for_current_stage?' do
      it 'returns false if the resource has no group label' do
        resource = resource_klass.new([])

        expect(resource).not_to be_has_category_label_for_current_stage
      end

      it 'returns true if the resource has a stage label and a corresponding category label' do
        resource = resource_klass.new([label_klass.new('devops::stage with two groups'), label_klass.new('Category:1')])

        expect(resource).to be_has_category_label_for_current_stage
      end

      it 'returns false if the resource has a stage label but no corresponding category label' do
        resource = resource_klass.new([label_klass.new('devops::stage with two groups'), label_klass.new('Category:5')])

        expect(resource).not_to be_has_category_label_for_current_stage
      end
    end

    describe '#has_category_label_for_current_group?' do
      it 'returns false if the resource has no group label' do
        resource = resource_klass.new([])

        expect(resource).not_to be_has_category_label_for_current_group
      end

      it 'returns true if the resource has a group label and a corresponding category label' do
        resource = resource_klass.new([label_klass.new('group::group1'), label_klass.new('Category:1')])

        expect(resource).to be_has_category_label_for_current_group
      end

      it 'returns false if the resource has a group label but no corresponding category label' do
        resource = resource_klass.new([label_klass.new('group::group1'), label_klass.new('Category:3')])

        expect(resource).not_to be_has_category_label_for_current_group
      end
    end

    describe '#can_infer_labels?' do
      let(:section_label) do
        label_klass.new(WwwGitLabCom.sections.first[1]['label'])
      end

      let(:stage_label) do
        label_klass.new(WwwGitLabCom.stages.first[1]['label'])
      end

      let(:group_label) do
        label_klass.new(WwwGitLabCom.groups.first[1]['label'])
      end

      let(:category_label) do
        label_klass.new(
          DevopsLabels.category_labels_per_group.dig(Hierarchy::Group.all.first.key, 0))
      end

      let(:category_label_from_other_group) do
        label_klass.new(
          DevopsLabels.category_labels_per_group.dig(Hierarchy::Group.all.last.key, 0))
      end

      it 'returns false if the resource has no stage, group, category, or team label' do
        resource = resource_klass.new([])

        expect(resource.can_infer_labels?).to be(false)
      end

      it 'returns false if it has all labels we may infer' do
        resource = resource_klass.new([
          stage_label, group_label, category_label, section_label
        ])

        expect(resource.can_infer_labels?).to be(false)
      end

      it 'returns true if it has all labels except category label for the particular group' do
        resource = resource_klass.new([
          stage_label, group_label, category_label_from_other_group, section_label
        ])

        expect(resource.can_infer_labels?).to be(true)
      end

      it 'returns true if the resource has a stage label' do
        resource = resource_klass.new([stage_label])

        expect(resource.can_infer_labels?).to be(true)
      end

      it 'returns true if the resource has a group label' do
        resource = resource_klass.new([group_label])

        expect(resource.can_infer_labels?).to be(true)
      end

      it 'returns true if the resource has a category label' do
        resource = resource_klass.new([category_label])

        expect(resource.can_infer_labels?).to be(true)
      end
    end

    describe '#can_infer_group_from_author?' do
      let(:resource_klass) do
        Struct.new(:author) do
          include DevopsLabels::Context
        end
      end

      let(:author) { 'johndoe' }
      let(:resource) { resource_klass.new(author) }

      before do
        allow(DevopsLabels).to receive(:group_for_user).with(author)
      end

      it 'returns true if there is a matching group for user' do
        allow(DevopsLabels).to receive(:group_for_user).and_return(true)

        expect(resource.can_infer_group_from_author?).to be(true)
      end

      it 'returns false if there is no matching group for user' do
        allow(DevopsLabels).to receive(:group_for_user).and_return(nil)

        expect(resource.can_infer_group_from_author?).to be(false)
      end
    end

    describe '#current_stage_has_a_single_group?' do
      it 'returns false if the resource has no stage label' do
        expect(resource.current_stage_has_a_single_group?).to be(false)
      end

      it 'returns true if the current stage has a single group' do
        resource = resource_klass.new([label_klass.new(Hierarchy::Stage.all_labels[1])])

        expect(resource.current_stage_has_a_single_group?).to be(true)
      end

      it 'returns false if the stage has several groups' do
        resource = resource_klass.new([label_klass.new(Hierarchy::Stage.all_labels.first)])

        expect(resource.current_stage_has_a_single_group?).to be(false)
      end
    end

    describe '#group_part_of_stage?' do
      let(:stage_label) { 'devops::stage with two groups' }
      let(:group_label) { 'group::group1' }
      let(:current_labels) { [stage_label, group_label] }
      let(:resource) { resource_klass.new(current_labels.map { |l| label_klass.new(l) }) }

      context 'when stage and group do not exist' do
        let(:current_labels) { [] }

        it 'returns false' do
          expect(resource.group_part_of_stage?).to be(false)
        end
      end

      context 'when stage does not exist' do
        let(:current_labels) { [group_label] }

        it 'returns false' do
          expect(resource.group_part_of_stage?).to be(false)
        end
      end

      context 'when group does not exist' do
        let(:current_labels) { [stage_label] }

        it 'returns false' do
          expect(resource.group_part_of_stage?).to be(false)
        end
      end

      context 'when group is not part of stage' do
        let(:group_label) { 'group::group3' }

        it 'returns false' do
          expect(resource.group_part_of_stage?).to be(false)
        end
      end

      context 'when group is part of stage' do
        it 'returns true' do
          expect(resource.group_part_of_stage?).to be(true)
        end
      end
    end

    describe '#first_group_for_current_stage' do
      it 'returns false if the resource has not stage label' do
        expect(resource.first_group_for_current_stage).to be_nil
      end

      it 'returns the first group of the current stage' do
        resource = resource_klass.new([label_klass.new(Hierarchy::Stage.all_labels.first)])

        expect(resource.first_group_for_current_stage.key).to eq('group1')
      end
    end

    describe '#stage_for_group' do
      it 'returns nil if the no stage corresponds to the given group' do
        expect(resource.stage_for_group(nil)).to be_nil
        expect(resource.stage_for_group('')).to be_nil
        expect(resource.stage_for_group('foo')).to be_nil
      end

      it 'returns the stage name of the given group' do
        expect(resource.stage_for_group('group1').key).to eq('stage_with_two_groups')
      end
    end

    describe '#comment_for_intelligent_stage_and_group_labels_inference' do
      where(:case_name, :current_labels, :expected_new_stage_and_group_labels_from_intelligent_inference, :explanation) do
        [
          ["stage: yes, group: yes, category: yes, team: no => Only section.", ["devops::stage with two groups", "group::group1", "Category:1"], ["section::section1"], ''],

          ["stage: yes, group: yes, category: no, team: no => Only section.", ["devops::stage with two groups", "group::group1"], ["section::section1"], ''],
          ["stage: yes, group: yes (doesn't exist in stages.yml), category: no, team: no => No inference", ["section::ci", "devops::monitor", "group::foo bar baz"], [], ''],

          ["stage: yes, group: no, category: yes, team: no (100% matching stage) => Group based on feature since feature matches stage", ["devops::stage with two groups", "Category:1"], ["extra-label", "group::group1", "section::section1"], %(Setting label(s) ~"extra-label" ~"group::group1" ~"section::section1" based on ~"Category:1" ~"group::group1".)],
          ["stage: yes, group: no, category: yes, team: no (100% matching stage, 2 different groups) => Manual triage required", ["devops::stage with two groups", "Category:1", "Category:3"], ["section::section1"], ''],
          ["stage: yes, group: no, category: yes, team: no (66% matching stage) => Group based on feature since feature matches stage", ["devops::stage with two groups", "Category:1", "something", "feature1"], ["extra-label", "group::group1", "section::section1"], %(Setting label(s) ~"extra-label" ~"group::group1" ~"section::section1" based on ~"Category:1" ~"feature1" ~"group::group1".)],
          ["stage: yes, group: no, category: yes, team: no (50% matching stage) => Group based on feature since feature matches stage", ["devops::stage with two groups", "Category:1", "something"], ["extra-label", "group::group1", "section::section1"], %(Setting label(s) ~"extra-label" ~"group::group1" ~"section::section1" based on ~"Category:1" ~"group::group1".)],
          ["stage: yes, group: no, category: yes, team: no (50% matching stage) => Group based on stage since feature does not match stage and stage has only one group", ["devops::stage_with_one_group", "something", "Category:5"], ["group::group3", "section::section2"], %(Setting label(s) ~"group::group3" ~"section::section2" based on ~"Category:5" ~"group::group3".)],
          ["stage: yes, group: no, category: yes, team: no (33% matching stage) => Group based on feature since feature matches stage", ["devops::stage with two groups", "Category:1", "something", "else"], ["extra-label", "group::group1", "section::section1"], %(Setting label(s) ~"extra-label" ~"group::group1" ~"section::section1" based on ~"Category:1" ~"group::group1".)],
          ["stage: yes, group: no, category: yes, team: no (33% matching stage) => Manual triage required since feature is less than 50%", ["devops::stage with two groups", "Category:1", "Category:3", "Category:5"], ["section::section1"], ''],
          ["stage: yes, group: no, category: yes, team: no (33% matching stage) => Group based on stage since feature does not match stage and stage has only one group", ["devops::stage_with_one_group", "Category:3"], ["group::group3", "section::section2"], %(Setting label(s) ~"group::group3" ~"section::section2" based on ~"devops::stage_with_one_group" ~"group::group3".)],
          ["stage: yes, group: no, category: yes, team: no (none matching stage) => Manual triage required since feature does not match stage but stage has several groups", ["devops::stage with two groups", "Category:Category5"], ["section::section1"], ''],
          ["stage: yes, group: no, category: yes, team: no (none matching stage) => Group based on stage since feature does not match stage and stage has only one group", ["devops::stage_with_one_group", "Category:Category1"], ["group::group3", "section::section2"], %(Setting label(s) ~"group::group3" ~"section::section2" based on ~"devops::stage_with_one_group" ~"group::group3".)],
          ["stage: yes, group: no, category: yes, team: no (none matching stage) => Group based on category since group has only one category", ["devops::stage_with_one_group", "Category:5"], ["group::group3", "section::section2"], %(Setting label(s) ~"group::group3" ~"section::section2" based on ~"Category:5" ~"group::group3".)],

          ["stage: yes, group: no, category: no, team: no => Manual triage required", ["devops::stage with two groups"], ["section::section1"], ''],
          ["stage: yes, group: no, category: no, team: no => Group based on stage since stage has a single group", ["devops::stage_with_one_group"], ["group::group3", "section::section2"], %(Setting label(s) ~"group::group3" ~"section::section2" based on ~"devops::stage_with_one_group" ~"group::group3".)],

          ["stage: no, group: yes, category: yes, team: no => Stage based on group", ["Category:1", "group::group1"], ["devops::stage with two groups", "section::section1"], %(Setting label(s) ~"devops::stage with two groups" ~"section::section1" based on ~"group::group1".)],

          ["stage: no, group: yes, category: no, team: no => Stage based on group and group has only one category", ["group::group3"], ["Category:5", "devops::stage_with_one_group", "section::section2"], %(Setting label(s) ~"Category:5" ~"devops::stage_with_one_group" ~"section::section2" based on ~"group::group3".)],

          ["stage: no, group: no, category: no, team: no => Manual triage required", ["bug", "rake tasks"], [], ''],

          ["stage: no, group: no, category: yes, team: no (best match: 100%) => Stage and group based on category", ["Category:1"], ["devops::stage with two groups", "extra-label", "group::group1", "section::section1"], %(Setting label(s) ~"devops::stage with two groups" ~"extra-label" ~"group::group1" ~"section::section1" based on ~"Category:1" ~"group::group1".)],
          ["stage: no, group: no, category: yes, team: no (best match: 66%) => Manual triage required", ["Category:1", "Category:3", "Category:5"], [], ''],
          ["stage: no, group: no, category: yes, team: no (best match: 50%) => Manual triage required", ["Category:1", "Category:3"], [], ''],
          ["stage: no, group: no, category: yes, team: no (best match: 33%) => Manual triage required", ["Category:1", "Category:5", "Category:category_with_no_group"], [], ''],
          ["stage: no, group: no, category: yes, team: no (no match) => Manual triage required", ["backstage"], [], ''],

          ["~QA => ~Quality", ["QA"], ["Quality"], %(Setting label(s) ~"Quality" based on ~"QA".)],
          ["~Quality ~QA => No new labels", %w[Quality QA], [], ''],

          ["~static analysis => ~Engineering Productivity", ["static analysis"], ["Engineering Productivity"], %(Setting label(s) ~"Engineering Productivity" based on ~"static analysis".)],
          ["~Engineering Productivity ~static analysis => No new labels", ["Engineering Productivity", "static analysis"], [], ''],

          ["stage: yes, group: yes, category: no, single_category: no => Apply Section label based on group, cannot infer Category based on group", ["group::group1", "devops::stage with two groups"], ["section::section1"], %(Setting label(s) ~"section::section1" based on ~"group::group1".)],
          ["stage: yes, group: yes (not part of the stage), category: no, single_category: no => No new labels", ["group::group1", "devops::stage_with_one_group"], [], ''],
          ["stage: no, group: yes, category: no, single_category: no => Apply Stage based on group, cannot infer Category based on group", ["group::group1"], ["devops::stage with two groups", "section::section1"], %(Setting label(s) ~"devops::stage with two groups" ~"section::section1" based on ~"group::group1".)],
          ["stage: yes, group: yes, category: no, single_category: no => Stage and Group already applied, cannot infer Category based on group. Do nothing", ["group::group1", "devops::stage with two groups"], ["section::section1"], ''],
          ["stage: yes, group: yes, category: no, single_category: no => Stage and Group already applied, cannot infer Category based on group. Do nothing", ["group::group1", "devops::stage with two groups"], ["section::section1"], ''],
          ["stage: no, group: yes, category: no => Apply Stage and Section labels", ["group::group1"], ["devops::stage with two groups", "section::section1"], %(Setting label(s) ~"devops::stage with two groups" ~"section::section1" based on ~"group::group1".)]
        ]
      end

      it "returns nil if the resource does not warrant any new labels" do
        resource = resource_klass.new([])

        expect(resource.comment_for_intelligent_stage_and_group_labels_inference).to be_nil
      end

      with_them do
        it "returns a comment with a /label quick action" do
          resource = resource_klass.new(current_labels.map { |l| label_klass.new(l) })
          label_action = "/label #{expected_new_stage_and_group_labels_from_intelligent_inference.map { |l| %(~"#{l}") }.join(' ')}"

          if expected_new_stage_and_group_labels_from_intelligent_inference.empty?
            expect(resource.comment_for_intelligent_stage_and_group_labels_inference).to be_nil
          else
            expect(resource.comment_for_intelligent_stage_and_group_labels_inference).to eq(label_action)
          end
        end
      end
    end

    describe '#comment_for_mr_author_group_label' do
      let(:resource_klass) do
        Struct.new(:author) do
          include DevopsLabels::Context
        end
      end

      let(:resource) { resource_klass.new(author) }

      context 'when author belongs to one group' do
        let(:author) { 'p-2' }

        it 'returns comment with a /label quick action' do
          expect(resource.comment_for_mr_author_group_label).to eq(%(/label ~"extra-label" ~"group::group1"))
        end
      end

      context 'when author belongs to multiple groups' do
        let(:author) { 'p-1' }

        it 'returns nil' do
          expect(resource.comment_for_mr_author_group_label).to be_nil
        end
      end
    end
  end
end
