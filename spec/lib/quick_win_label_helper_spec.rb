# frozen_string_literal: true

require 'spec_helper'
require_relative '../../lib/quick_win_label_helper'

RSpec.describe QuickWinLabelHelper do
  let(:dummy_class) { Class.new { include QuickWinLabelHelper } }
  let(:instance) { dummy_class.new }

  it 'includes Triage::QuickWin::LabelValidator' do
    expect(dummy_class.included_modules).to include(QuickWin::LabelValidator)
  end

  it 'includes Triage::QuickWin::MessageFormatter' do
    expect(dummy_class.included_modules).to include(QuickWin::MessageFormatter)
  end
end
