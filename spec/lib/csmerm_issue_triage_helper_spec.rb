# frozen_string_literal: true

require 'spec_helper'
require_relative '../../lib/csmerm_issue_triage_helper'

RSpec.describe CsmermIssueTriageHelper do
  let(:resource_with_all_attributes) do
    {
      assignees: ['@gavinpeltz'],
      'labels' => [
        'Priority::Top-5',
        'Team::CSM',
        'SP Objective::Status::On Track',
        'CS Region::AMER',
        'OKR::Yes',
        'CSLT::Kent',
        'FY24-Q1',
        'CSXLT::Oliver'
      ]
    }
  end

  let(:resource_with_no_attributes) do
    {
      assignees: [],
      'labels' => []
    }
  end

  describe '.csmerm_issue_triage_automated_comment_table' do
    context 'when all requirements are met' do
      it 'returns a table with only headers' do
        expected_table = <<~MARKDOWN.chomp
          | Missing Field | Example |
          |---------------| ------- |

        MARKDOWN

        expect(described_class.csmerm_issue_triage_automated_comment_table(resource_with_all_attributes)).to eq(expected_table)
      end
    end

    context 'when no requirements are met' do
      it 'returns a table with all missing requirements' do
        result = described_class.csmerm_issue_triage_automated_comment_table(resource_with_no_attributes)

        # Verify table headers
        expect(result).to include("| Missing Field | Example |")
        expect(result).to include("|---------------| ------- |")

        # Verify all requirements are listed
        expect(result).to include("| Issue assignee | `@gavinpeltz` |")
        expect(result).to include('| Priority label | ~"Priority::Top-5" |')
        expect(result).to include('| Team label | ~"Team::CSM" |')
        expect(result).to include('| Status/At Risk label | ~"SP Objective::Status::On Track" |')
        expect(result).to include('| Region label | ~"CS Region::AMER" |')
        expect(result).to include('| OKR label | ~"OKR::Yes" |')
        expect(result).to include('| LT Sponsorship label | ~"CSLT::Kent" |')
        expect(result).to include('| Fiscal quarter label | ~"FY24-Q1" |')
        expect(result).to include('| XLT DRI label | ~"CSXLT::Oliver" |')
      end
    end
  end

  describe '.missing_csmerm_issue_attributes' do
    context 'when all requirements are met' do
      it 'returns an empty array' do
        expect(described_class.missing_csmerm_issue_attributes(resource_with_all_attributes)).to be_empty
      end
    end

    context 'when no requirements are met' do
      it 'returns all missing requirements' do
        missing_attributes = described_class.missing_csmerm_issue_attributes(resource_with_no_attributes)

        expect(missing_attributes).to include("| Issue assignee | `@gavinpeltz` |")
        expect(missing_attributes).to include('| Priority label | ~"Priority::Top-5" |')
        expect(missing_attributes).to include('| Team label | ~"Team::CSM" |')
        expect(missing_attributes).to include('| Status/At Risk label | ~"SP Objective::Status::On Track" |')
        expect(missing_attributes).to include('| Region label | ~"CS Region::AMER" |')
        expect(missing_attributes).to include('| OKR label | ~"OKR::Yes" |')
        expect(missing_attributes).to include('| LT Sponsorship label | ~"CSLT::Kent" |')
        expect(missing_attributes).to include('| Fiscal quarter label | ~"FY24-Q1" |')
        expect(missing_attributes).to include('| XLT DRI label | ~"CSXLT::Oliver" |')
      end
    end

    context 'with partial requirements' do
      let(:resource_with_partial_attributes) do
        {
          assignees: ['@gavinpeltz'],
          'labels' => [
            'Priority::Top-5',
            'Team::CSM'
          ]
        }
      end

      it 'returns only missing requirements' do
        missing_attributes = described_class.missing_csmerm_issue_attributes(resource_with_partial_attributes)

        # Should not include these as they are present
        expect(missing_attributes).not_to include("| issue assignee | `@gavinpeltz` |")
        expect(missing_attributes).not_to include('| Priority label | ~"Priority::Top-5" |')
        expect(missing_attributes).not_to include('| Team label | ~"Team::CSM" |')

        # Should include these as they are missing
        expect(missing_attributes).to include('| Status/At Risk label | ~"SP Objective::Status::On Track" |')
        expect(missing_attributes).to include('| Region label | ~"CS Region::AMER" |')
        expect(missing_attributes).to include('| OKR label | ~"OKR::Yes" |')
        expect(missing_attributes).to include('| LT Sponsorship label | ~"CSLT::Kent" |')
        expect(missing_attributes).to include('| Fiscal quarter label | ~"FY24-Q1" |')
        expect(missing_attributes).to include('| XLT DRI label | ~"CSXLT::Oliver" |')
      end
    end

    context 'with alternative valid labels' do
      let(:resource_with_alternative_labels) do
        {
          assignees: ['@someone'],
          'labels' => [
            'Priority::1',
            'Team::CSA',
            'SP Objective::Status::At Risk',
            'CS Region::EMEA',
            'OKR::No',
            'CSLT::Someone',
            'FY23-Q4',
            'CSXLT::Someone'
          ]
        }
      end

      it 'recognizes all valid label variations' do
        expect(described_class.missing_csmerm_issue_attributes(resource_with_alternative_labels)).to be_empty
      end
    end
  end
end
