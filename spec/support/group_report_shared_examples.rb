# frozen_string_literal: true

RSpec.shared_context 'with report metadata' do
  let(:assignees) { ['@user1'] }
  let(:labels)    { %w[first-label second-label] }
  let(:mentions)  { ['@user2 @user3'] }
end

RSpec.shared_examples 'report summaries' do |assignees = "`@fake_be_em` `@fake_em` `@fake_fe_em` `@fake_fs_em` `@fake_pm`", group_labels = '~"group::group1" ~"extra-label"'|
  include_context 'with report metadata'

  describe 'labels' do
    it 'sets some default labels' do
      expect(subject).to match(%r{/label .*~"triage report" ~"type::ignore"})
    end

    it 'sets the group labels' do
      expect(subject).to match(%r{/label .*#{group_labels}})
    end
  end

  context 'when assignees are present' do
    context 'when mentions are present' do
      it 'includes assignees (backticked)' do
        expect(subject).to match("Hi #{assignees},\n")
      end
    end
  end
end
