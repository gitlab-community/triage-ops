# frozen_string_literal: true

module StubGoogleSheetRequests
  # NOTE: This private key is generated and not real!
  GOOGLE_SERVICE_ACCOUNT_CREDS = <<-JSON
    {
      "type": "service_account",
      "project_id": "abc123",
      "private_key_id": "1111111111111111111111111111111111111111",
      "private_key": "-----BEGIN PRIVATE KEY-----\\nMIIBVAIBADANBgkqhkiG9w0BAQEFAASCAT4wggE6AgEAAkEAv4KNL8dZPL6h0tTP\\nL5WXti24v5osoVv4emCux6S2H4bu3ata8ppqHfcSsJSxiqlBq1sqqx3uVxkZDBuF\\nsJNE9QIDAQABAkBKRE+KUs15cBgDUcHTGzkNTifSLfDW1nrCwpGlHGwARz+3OwKl\\nSFPOVxAhzHdtaHjd/oGL50/qlp3PDJGNPAO1AiEA94o1SVjQ0NBLnJq4Lsm1yU0U\\nvHps+BN+O2jXfyXCXy8CIQDGDh90PZoBBrp5nL120ZeiPwkfObfIds9FP/bfOum1\\nGwIhAJhATJgJZZ4Zj3gJ/aDhdcsTet6WWjGXI7v8txbALbYHAiAaS3O3nhodOsR1\\nMu8goFEOdGoEoEgbMFLycbyYBJ1UswIgX7lVsqWb7WsCslr2kluFzP5aC9wMUvWG\\nUTSIXUN6mqk=\\n-----END PRIVATE KEY-----\\n",
      "client_email": "gitlab@example.com",
      "client_id": "111111111111111111111",
      "auth_uri": "https://accounts.google.com/o/oauth2/auth",
      "token_uri": "https://oauth2.googleapis.com/token",
      "auth_provider_x509_cert_url": "https://www.googleapis.com/oauth2/v1/certs",
      "client_x509_cert_url": "https://www.googleapis.com/robot/v1/metadata/x509/gitlab%40example.iam.gserviceaccount.com",
      "universe_domain": "googleapis.com"
    }
  JSON

  def stub_googleapis_auth_request(credential_path)
    stub_request(:post, "https://www.googleapis.com/oauth2/v4/token").to_return(status: 200, body: "{}", headers: { 'Content-Type' => 'application/json' })

    file = instance_double(File, read: GOOGLE_SERVICE_ACCOUNT_CREDS)
    allow(File).to receive(:open).with(credential_path).and_return(file)
  end

  def stub_read_mrarr_organization_request
    stub_request(:get, "https://sheets.googleapis.com/v4/spreadsheets/#{sheet_id}/values/MRARR%20Organization")
      .to_return(
        status: 200,
        body: '{"range":"MRARR Organization!A1:C1","majorDimension":"ROWS","values":[["CONTRIBUTOR_ORGANIZATION","Issue link","CONTRIBUTOR_USERNAMES"], ["Org 1","http://link_1.com","[ \n  \"username_1\", \n  \"username_2\"\n ]"], ["Org 2","","[ \n  \"username_3\", \n  \"username_4\"\n ]"]]}',
        headers: { 'Content-Type' => 'application/json' })
  end

  def stub_read_leading_organization_users_request
    stub_request(:get, "https://sheets.googleapis.com/v4/spreadsheets/#{sheet_id}/values/Leading%20Organization%20Users")
      .to_return(
        status: 200,
        body: '{"range":"Leading Organization Users!A1:C1","majorDimension":"ROWS","values":[["username"],["leader"],["another_leader"],["final.leader"]]}',
        headers: { 'Content-Type' => 'application/json' })
  end
end
