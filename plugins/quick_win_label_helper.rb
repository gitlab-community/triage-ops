# frozen_string_literal: true

require_relative '../lib/quick_win_label_helper'

Gitlab::Triage::Resource::Context.include QuickWinLabelHelper
