# frozen_string_literal: true

require 'httparty'
require 'yaml'
require_relative '../lib/ai_triage_helper'
require_relative '../lib/www_gitlab_com'

module IssueTriageHelper
  def issue_triage_ai_comment
    AiTriageHelper.new(groups_and_categories).execute(title, description, issue_labels, author)
  end

  private

  def title
    resource[:title]
  end

  def description
    resource[:description]
  end

  def project_id
    resource[:project_id]
  end

  def author
    resource.dig(:author, :username)
  end

  def issue_labels
    resource[:labels]
  end

  def groups_and_categories
    @groups_and_categories ||= parse_groups_and_categories
  end

  def parse_groups_and_categories
    groups_and_categories = {}

    WwwGitLabCom.groups.each_value do |group|
      group_label = group['label']

      group['categories'].each do |category_name|
        category_label = WwwGitLabCom.categories[category_name]&.[]('label')
        existing_categories = groups_and_categories[group_label] || []
        groups_and_categories[group_label] = existing_categories + [category_label]
      end
    end

    groups_and_categories
  end
end

Gitlab::Triage::Resource::Context.include IssueTriageHelper
