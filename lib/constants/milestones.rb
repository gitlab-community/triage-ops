# frozen_string_literal: true

module Milestones
  VERSIONED_MILESTONE_PATTERN = /\A\d+\.\d+\z/ # So we don't pick 2019
end
