# frozen_string_literal: true

require_relative '../triage/triage'
require_relative '../triage/triage/event'
require_relative 'hierarchy/group'
require_relative 'team_member_helper'

module MergeRequestCoachHelper
  include TeamMemberHelper

  private

  def coach
    @coach ||= select_random_merge_request_coach(group: group&.name, role: role)
  end

  def group
    @group ||= Hierarchy::Group.find_by_label(group_label)
  end

  def role
    label_names.grep(Labels::FRONTEND_LABEL).any? ? /frontend/ : nil
  end

  def group_label
    event.group_label
  end

  def label_names
    event.label_names
  end
end
