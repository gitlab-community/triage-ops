# frozen_string_literal: true

require 'json'
require_relative 'anthropic_api'

class AiTriageHelper
  LABELS = {
    types: ["type::bug", "type::feature", "type::maintenance"],
    subtypes: [
      "feature::addition", "feature::enhancement", "feature::consolidation",
      "bug::mobile", "bug::transient", "bug::functional", "bug::performance", "bug::availability", "bug::vulnerability", "bug::ux",
      "maintenance::release", "maintenance::removal", "maintenance::performance", "maintenance::test-gap", "maintenance::workflow", "maintenance::refactor", "maintenance::usability", "maintenance::pipelines", "maintenance::dependency", "maintenance::scalability"
    ],
    areas: %w[backend frontend database documentation UX]
  }.freeze

  def initialize(groups_and_categories)
    @groups_and_categories = groups_and_categories
  end

  def execute(title, description, labels, author)
    anthropic_response = AnthropicApi.execute(system_prompt, prompt(title, description, labels))
    generate_comment(JSON.parse(anthropic_response), author)
  end

  private

  attr_reader :groups_and_categories

  def validate_label(detail)
    return if detail['deduced'].nil?
    return if detail['deduced'] == detail['existing']
    return if detail['confidence'] < 80

    detail['deduced']
  end

  def generate_comment(metadata, author)
    ai_note = metadata['note']
    ai_note = "\nHere's what our AI has to say:\n\n> #{ai_note}\n" unless ai_note == ''

    labels = [
      'automation:ml',
      validate_label(metadata['category']),
      validate_label(metadata['group']),
      validate_label(metadata['type']),
      validate_label(metadata['subtype'])
    ].compact

    areas = validate_label(metadata['areas'])
    labels += areas if areas

    <<~NOTE
      Hi @#{author}, thank you for creating this issue!

      We've attempted to automatically add labels to help route your issue to the right team.
      #{ai_note}
      We believe everyone can contribute and welcome you to work on this proposed change, feature or bug fix.
      If you are a new community contributor, start your [onboarding journey](https://contributors.gitlab.com) and get to know our community.

      Team members, please indicate any mistakes by adding `/label ~"automation:ml wrong"`.

      /label ~"#{labels.join('" ~"')}"
      <details><summary>View AI response JSON</summary>

      ```json
      #{JSON.pretty_generate(metadata)}
      ```
      </details>

      ---

      [This message was generated automatically using AI](https://gitlab.com/gitlab-community/quality/triage-ops/-/blob/master/policies/stages/report/untriaged-issues.yml).
      We strive for accuracy and quality, but please note the information may not be error-free.
    NOTE
  end

  def system_prompt
    <<~PROMPT
      Based on the following GitLab issue details, select the most relevant labels from the predefined lists
      (only one label from each list, and only labels from the list).

      Groups and categories:
      #{groups_and_categories}

      Types: #{LABELS[:types].join(', ')}
      Subtypes: #{LABELS[:subtypes].join(', ')}
      Areas: #{LABELS[:areas].join(', ')}

      Please return the selected labels in the following JSON format without any other text/explanation:
      {
        "category": {
          "existing":,
          "deduced":,
          "confidence":
        },
        "group": {
          "existing":,
          "deduced":,
          "confidence":
        },
        "type": {
          "existing":,
          "deduced":,
          "confidence":
        },
        "subtype": {
          "existing":,
          "deduced":,
          "confidence":
        },
        "areas": {
          "existing": [],
          "deduced": [],
          "confidence":
        },
        "note":
      }

      If you believe you have determined the subtype, the type must match.
      If you are unsure of the subtype, please only select a type.

      If you believe you have determined a category, the group must match.
      If you are unsure of the category, please only select a group.

      Confidence should be a number between 0 and 100.
      Please ignore any existing labels prefixed with `automation:`.

      In the note field please state if you have found any inconsistencies between the existing category, group, type, subtype and area labels.
      Do not state if there are no inconsistencies or if existing labels are not included in the provided label lists.
    PROMPT
  end

  def prompt(title, description, labels)
    <<~PROMPT
      Here are the details of the GitLab issue you need to analyze:

      <title>
      #{title}
      </title>

      <description>
      #{description}
      </description>

      <labels>
      #{labels.join(', ')}
      </labels>
    PROMPT
  end
end
